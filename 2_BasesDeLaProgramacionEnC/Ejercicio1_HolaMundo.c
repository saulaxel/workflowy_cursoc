/*
 * Programa que imprime en la pantalla un mensaje sencillo
 * "Hola, Mundo"
 */

// ##### Sección de declaraciones e inclusión de bibliotecas #####

#include <stdio.h> // Cabecera para realizar operaciones de entrada y salida como printf y scanf
                   // Su nombre significa STDandard Input Output


// ##### Función principal #####
int main(void) // La función principal siempre regresa un "int" y puede o no tener argumentos
{
    // La función printf muestra un mensaje en pantalla. Dicha función significa
    // PRINT Formatted

    printf("Hola, Mundo\n"); // El mensaje a mostrar en pantalla
                             //  "\n" es un salto de línea

    return 0; // Indica explícitamente que el programa termina con éxito
              // Esta línea se puede omitir en programas modernos (C99 para adelante)

} // Fin de la función principal
