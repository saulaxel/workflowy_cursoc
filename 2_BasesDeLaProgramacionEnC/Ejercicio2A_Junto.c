/*
 * Imprimir las palabras "ola", "k" y "ace" en tres
 * instrucciones diferentes sin separación
 */

#include <stdio.h>  // Para usar printf()

// Función principal
int main(void)
{
    printf("ola");
    printf("k");
    printf("ace");
}
