/*
 * Imprimir las palabras "ola", "k" y "ace" en tres
 * instrucciones diferentes separándolas por tabulacíones
 */

#include <stdio.h>  // Para usar printf()

// Función principal
int main(void)
{
    printf("ola\t");
    printf("k\t");
    printf("ace\t");
}

