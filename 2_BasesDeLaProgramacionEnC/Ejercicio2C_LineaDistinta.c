/*
 * Imprimir las palabras "ola", "k" y "ace" en tres
 * instrucciones diferentes y en diferentes líneas
 */

#include <stdio.h>  // Para usar printf()

// Función principal
int main(void)
{
    printf("ola\n");
    printf("k\n");
    printf("ace\n");
}

