/*
 * Un pequeño ejemplo del "Retorno de carro", una de las
 * secuencias de escape menos usadas de C
 */

// Cabeceras del programa
#include <stdio.h>  // Para usar printf() y fflush()
#include <stdlib.h> // Para usar la función system()

// Hay código que no funciona igual en todos los sistemas operativos.
// Este tipo de código recibe el nombre de "No Estándar".

// A continuación incluimos un pequeño código de compatibilidad para poder
// hacer que nuestro programa funcione igual en windows, linux o mac; Este código
// hace uso de los "#if" (gatito if o condicional de preprocesador) para revisar en qué
// tipo de sistema operativo estamos

#ifdef _WIN32 // Si estamos en windows

#include <conio.h>  // Para la función getch() en windows
void EsperarTecla(void)
{
	getch(); // getch() regresa la tecla presionada y no espera hasta que se presione Enter
}
#else // Si no es windows, el sistema será seguramente algún unix
void EsperarTecla(void)
{
	system("stty -echo +raw"); // Esta instrucción hace que las lecturas de valores no esperen hasta presionar enter
	getchar(); // getchar() generalmente espera a que se presione enter, pero esta vez no gracias a la línea anterior
	system("stty +echo -raw"); // Regresamos la configuración a su estado normal
}
#endif

// Función main
int main(void)
{

	for (int i = 1; i <= 100; i++)
	{
		printf("\rEl número es: %3d. Presiona una tecla para aumentar el valor", i);
		EsperarTecla();
	}

} // Fin de la función main