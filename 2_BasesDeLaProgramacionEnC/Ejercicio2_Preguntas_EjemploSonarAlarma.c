/*
 * Ejemplo de como usar la secuencia de escape "\a"
 * para hacer sonar una pequeña alarma.
 */

#include <stdio.h> // Para usar puts, getchar

int main(void)
{
	// Puts es como printf, pero inserta un salto de líenea al final siempre
	// y no puede imprimir variables
	puts("Presione enter para hacer sonar la alarma");

	getchar(); // Espera a que se presione enter

	puts("\a\a\a\a\a"); // Sonar la alarma 5 veces seguidas
	puts("\t\t ***** ERROR! ERROR! ***** ");
}