/*
 * Haciendo un ejemplo de uso de la tabulación vertical
 * "\v", la cual es poco usada y no funciona en windows.
 */

int main(void)
{
	// Cada palabra se imprime saparada por una nueva tabulación vertical
	puts("¿Qué\vPasará\vQué\vMisterio\vHabrá?");
}