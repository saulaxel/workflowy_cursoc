/*
 * Mostrar como realizar las operaciones básicas
 * en lenguaje C
 */

#include <stdio.h> // para usar puts(), printf() y scanf()

// Función principal
int main(void)
{
    // Escribimos un pequeño letrero en pantalla
    puts("\t====================\n"
         "\t= >> Calculator << =\n"
         "\t====================\n");

    float val1, val2; // Valores que se pedirán al usuario
    printf(" Ingrese el primer valor: ");
    scanf("%f", &val1); // Se lee el primer valor

    printf(" Ingrese el segundo valor: ");
    scanf("%f", &val2); // Se lee el segundo valor

    // Imprimiendo los resultados en un letrero
    // Nótese que no se guardó cada resultado en una
    // Variable distinta
    puts  (" ╔══════════════════════════╗\n"
           " ║ Resultados:              ║");
    printf(" ║ %14s: %8.2f ║\n", "Suma",           val1 + val2);
    printf(" ║ %14s: %8.2f ║\n", "Resta",          val1 - val2);
    printf(" ║ %14s: %8.2f ║\n", "Multiplicación", val1 * val2);
    printf(" ║ %14s: %8.2f ║\n", "División",       val1 / val2);
    puts  (" ╚=═════════════════════════╝\n");
}