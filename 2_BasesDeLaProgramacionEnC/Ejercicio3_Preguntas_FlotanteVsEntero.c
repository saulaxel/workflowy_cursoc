/*
 * Comparación entre números enteros y
 * flotantes
 */

#include <stdio.h>  // Para usar puts(), printf()
#include <math.h>   // Para usar fmod(); Compilar con la bandera "-l m"

// Función principal
int main(void)
{
    // ##### División exacta #####
    if (5 / 2 == 2.5)
    {
        puts("Los enteros tienen división exacta");
    }
    else
    {
        puts("Los enteros truncan la división");
    }

    if (5.0 / 2.0 == 2.5)
    {
        puts("Los flotantes tienen división exacta");
    }
    else
    {
        puts("Los flotantes truncan la división");
    }

    // ##### Propiedad asociativa #####
    if (1 + (2 + 3) == (1 + 2) + 3)
    {
        puts("Los enteros cumplen la propiedad asociativa");
    }
    else
    {
        puts("Los enteros NO cumplen la propiedad asociativa");
    }

    if (0.1 + (0.2 + 0.3) == (0.1 + 0.2) + 0.3)
    {
        puts("Los flotantes cumplen la propiedad asociativa");
    }
    else
    {
        puts("Los flotantes NO cumplen la propiedad asociativa");
    }

    // ##### Obtener el residuo #####
    int residuo_entero = 5 % 2;
    printf("El residuo (entero) de 5 / 2 es: %d\n", residuo_entero);
    printf("Dicho residuo se obtiene con el operador \"%%\"\n");

    float residuo_flotante = fmod(8.0, 1.5);
    printf("El residuo (flotante) de 8.0 / 1.5 es: %f\n", residuo_flotante);
    printf("Dicho residuo se obtiene con la función fmod() de math.h\n");
}

