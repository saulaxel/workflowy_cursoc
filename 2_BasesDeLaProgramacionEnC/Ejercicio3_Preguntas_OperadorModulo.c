/*
 * Demostrando algunas propiedades del operador módulo
 */

#include <stdio.h>  // Para poder usar puts(), printf()

int main(void)
{
    puts("\t\t   .: El operador Modulo :.    \n"
         "\t\t .::::::::::::::::::::::::::.  \n");

    puts("El operador módulo en números positivos obtiene el residuo\n"
         "de la división");
    printf("En la división 7 / 2, el cociente es %d y el residuo es %d\n",
            7 / 2, 7 % 2);

    puts("El operador módulo sirve para hacer operaciones de forma\n"
         "periódica dentro de ciclos");
    for (int i = 1; i <= 100; i++)
    {
        printf("%3d ", i); // Se imprime i, cuyo valor va de 1 a 100
        if (i % 5 == 0) // Cada cinco números se imprime un salto de línea
        {
            printf("\n");
        }
    }
}
