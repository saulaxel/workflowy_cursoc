/*
 * Programa que calcula el sueldo mensual de un trabajador
 * que cobra una cuota fija por día pero trabaja una cantidad variable
 * de días
 */

// ##### Declaraciones #####
#include <stdio.h>

#define SUELDO_DIARIO (300.0) // El sueldo del trabajador se declara como constante simbólica
                              // De esta forma no se alterará el valor

// Función principal
int main(void)
{
    puts("\t Bienvenido, Usuario \n"
         "\t ------------------- \n");

    int dias_trabajados = 0; // Variable para los días trabajados inicializada a 0
    printf("¿Cuántos días ha trabajado este mes?\n"
           " > ");
    scanf("%d", &dias_trabajados);

    float sueldo = SUELDO_DIARIO * dias_trabajados;

    // Se muestra el sueldo con dos decimales de precisión
    printf("Parece que su sueldo es: %.2f\n", sueldo);
}