/*
 * Programa que calcula el sueldo mensual de un trabajador
 * que trabaja una cantidad fija de días pero tiene un sueldo
 * variable
 */

// ##### Declaraciones #####
#include <stdio.h>

#define DIAS_TRABAJADOS (20)  // El sueldo del trabajador se declara como constante simbólica
                              // De esta forma no se alterará el valor

// Función principal
int main(void)
{
    puts("\t Bienvenido, Usuario \n"
         "\t ------------------- \n");

    float sueldo_diario = 0; // Variable para el sueldo diario del usuario
    printf("¿Cuál es el monto de tu sueldo?\n"
           " > ");
    scanf("%f", &sueldo_diario);

    float sueldo = sueldo_diario * DIAS_TRABAJADOS;

    // Se muestra el sueldo con dos decimales de precisión
    printf("Parece que su sueldo es: %.2f\n", sueldo);
}