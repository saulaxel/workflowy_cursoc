/*
 * Programa que calcula el sueldo mensual de un trabajador
 * si se ingresa el sueldo por día y los días trabajados.
 */

// ##### Declaraciones #####
#include <stdio.h>

// Función principal
int main(void)
{
    puts("\t Bienvenido, Usuario \n"
         "\t ------------------- \n");

    int dias_trabajados = 0; // Variable para los días trabajados inicializada a 0
    float sueldo_diario = 0.0; // Variable para guardar el sueldo diario
    printf("¿Cuántos días ha trabajado este mes?\n"
           " > ");
    scanf("%d", &dias_trabajados);

    printf("¿Cuál es su sueldo por día?\n"
           " > ");
    scanf("%d", &sueldo_diario);

    float sueldo = sueldo_diario * dias_trabajados;

    // Se muestra el sueldo con dos decimales de precisión
    printf("Parece que su sueldo es: %.2f\n", sueldo);
}