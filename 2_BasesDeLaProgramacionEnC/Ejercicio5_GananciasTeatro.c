/*
 * Programa que calcula la ganancia de una puesta en escena
 * a partir de la cantidad de niños y adultos que ingresan a
 * un teatro
 */

// ##### Sección de declaraciones ######
#include <stdio.h>   // para printf(), puts(), scanf()
#include <stdlib.h>  // Para usar EXIT_SUCCESS y EXIT_FAILURE
#include <stdbool.h> // Alias para _Bool

#define PRECIO_GRANDES (40.0)
#define PRECIO_CHICOS  (20.0)

// ##### Función principal #####
int main(void)
{
    puts("\t* * * * * * * * * * * * * * * * * * * * * *\n"
         "\t * * Producciones Hermanos Menganitos  * * \n"
         "\t * *     - Sistema de inventario -     * * \n"
         "\t* * * * * * * * * * * * * * * * * * * * * *\n");

    // ##### Pidiendo los valores y comprobando que sean correctos #####
    int chicos, grandes;
    bool datos_validos;

    printf(" ¿Cuántos niños vieron la función? ");
    datos_validos = (scanf("%d", &chicos) == 1);

    if (!datos_validos || chicos < 0)
    {
        // datos_validos se asigna a false si no se leyó correctamente
        // un entero en la variable chicos. Además, no se debe ingresar
        // un valor negativo, pues no hay cantidades
        printf("El número de niños es inválido\n");
        return EXIT_FAILURE;
    }

    printf(" ¿Cuántos adultos vieron la función? ");
    datos_validos = (scanf("%d", &grandes) == 1);

    if (!datos_validos || grandes < 0)
    {
        printf("El número de niños es inválido\n");
        return EXIT_FAILURE;
    }

    putchar('\n'); // Salto de línea salvaje aparece

    // Calculando las ganancias y mostrándolas
    float total_chicos  = chicos * PRECIO_CHICOS;
    float total_grandes = grandes * PRECIO_GRANDES;

    printf(" | Monto por boletos de niños:       %6.2f |\n",
            total_chicos);
    printf(" | Monto por boletos de los grandes: %6.2f |\n",
            total_grandes);
    printf(" | Total:                            %6.2f |\n",
            total_chicos + total_grandes);

    return EXIT_SUCCESS;
}
