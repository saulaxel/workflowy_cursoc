/*
 * Calcular la hipotenusa de un triángulo a partir de la
 * longitud de sus catetos
 */

// ##### Preprocesador #####
#include <stdio.h>      // para printf(), scanf(), puts()
#include <stdlib.h>     // para EXIT_FAILURE, EXIT_SUCCESS
#include <stdbool.h>    // alias de _Bool
#include <math.h>       // para sqrt(), hypot()

// ##### Función principal #####
int main(void)
{
    double a, b, c; // Las variables 'double' son flotantes,
                    // pero con igual o mayor precisión que un 'float'
    bool valores_correctos;

    // ##### Leyendo los valores y validándolos #####
    printf("Ingrese las longitudes de los catetos\n");
    // Al leer 'double', se usa "%lf" en lugar de "%f"
    valores_correctos = (scanf("%lf%lf", &a, &b));

    if (!valores_correctos)
    {
        puts("La longitud de los catetos debe ser numérica");
        exit(EXIT_FAILURE);
    }

    if (a < 0 || b < 0)
    {
        puts("No existen las longitudes negativas >:v");
        exit(EXIT_FAILURE);
    }

    // ##### Calculando la hipotenusa y mostrando el resultado #####
    c = sqrt((a * a) + (b * b)); // Forma 1

    int n;
    printf("\n Hipotenusa por medio de la formula: %g\n", c);
    n = printf(" Hipotenusa por medio de la función hypot(): %g\n", c);

    // Se muestra una línea al final formada con guión bajo. El largo
    // de la línea crece conforme lo hace el mensaje
    for (int i = 0; i < n; i++) { putchar('_'); }

    putchar('\n'); // Salto de línea
    return EXIT_SUCCESS;
}
