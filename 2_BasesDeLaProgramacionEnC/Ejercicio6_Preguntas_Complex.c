/*
 * Algunas funciones matemáticas de la biblioteca complex
 */

#include <stdio.h>
#include <math.h>     // Para usar M_PI
#include <complex.h>

int main(void)
{
    float flotante;
    double doble;
    long double doble_largo;

    complex float cflotante;
    complex double cdoble;
    complex long double cdoble_largo;

    // Obtener parte real de un número complejo
    flotante    = crealf(0 + 0.5 * I);
    doble       = creal(0.5 + 0.5 * I);
    doble_largo = creall(0.5 + 0 * I);

    printf("Parte real de 0.0 + 0.5*i: %f\n", flotante);
    printf("Parte real de 0.5 + 0.5*i: %lf\n", doble);
    printf("Parte real de 0.5 + 0.0*i: %Lf\n", doble_largo);

    // Obtener parte imaginaria de un número complejo
    flotante    = cimagf(0 + 0.5 * I);
    doble       = cimag(0.5 + 0.5 * I);
    doble_largo = cimagl(0.5 + 0 * I);

    printf("Parte imaginaria de 0.0 + 0.5*i: %f\n", flotante);
    printf("Parte imaginaria de 0.5 + 0.5*i: %lf\n", doble);
    printf("Parte imaginaria de 0.5 + 0.0*i: %Lf\n", doble_largo);

    // Obtener conjugado complejo
    cflotante    = conjf(0 + 0.5 * I);
    cdoble       = conj(0.5 + 0.5 * I);
    cdoble_largo = conjl(0.5 + 0 * I);

    printf("Conjugado complejo de 0.0 + 0.5*i: %f%+f*i\n", crealf(cflotante), cimagf(cflotante));
    printf("Conjugado complejo de 0.5 + 0.5*i: %lf%+lf*i\n", creal(cdoble), cimag(cdoble));
    printf("Conjugado complejo de 0.5 + 0.0*i: %Lf%+Lf*i\n", creall(cdoble_largo), creall(cdoble_largo));

    // Valor absoluto complejo
    flotante    = cabsf(3.0f + 4.0f * I);
    doble       = cabs(5.0 + 12.0 * I);
    doble_largo = cabsl(65.0L + 72.0L * I);

    printf("Valor absoluto de 3.0+4.0*i: %f\n", flotante);
    printf("Valor absoluto de 5.0+12.0*i: %lf\n", doble);
    printf("Valor absoluto de 65.0+72.0*i: %Lf\n", doble_largo);

    // Argumento o angulo de fase de un número complejo
    flotante    = cargf(3.0f + 4.0f * I);
    doble       = carg(5.0 + 12.0 * I);
    doble_largo = cargl(65.0L + 72.0L * I);

    printf("Angulo de fase 3.0+4.0*i: %f\n", flotante);
    printf("Angulo de fase 5.0+12.0*i: %lf\n", doble);
    printf("Angulo de fase 65.0+72.0*i: %Lf\n", doble_largo);

    // Proyección en la esfera de Riemann de un número complejo
    flotante    = cprojf(0.5 + 0.0 * I);
    doble       = cproj(0.5 + 0.5 * I);
    doble_largo = cprojl(0.0 + 0.5 * I);

    printf("Proyección en la esfera de Riemann de 0.5+0.0*i: %f\n", flotante);
    printf("Proyección en la esfera de Riemann de 0.5+0.5*i: %lf\n", doble);
    printf("Proyección en la esfera de Riemann de 0.0+0.5*i: %Lf\n", doble_largo);

    // Funciones trigonométricas complejas
    cflotante    = csinf((0.2 * M_PI) + (0.5 * M_PI * I));
    cdoble       = csin((0.4 * M_PI) + (0.3 * M_PI * I));
    cdoble_largo = csinl((0.6 * M_PI) + (0.1 * M_PI * I));

    printf("El seno complejo de 0.2pi + 0.5pi*i: %f%+f*i\n", crealf(cflotante), cimagf(cflotante));
    printf("El seno complejo de 0.4pi + 0.3pi*i: %lf%+lf*i\n", creal(cdoble), cimag(cdoble));
    printf("El seno complejo de 0.6pi + 0.1pi*i: %Lf%+Lf*i\n", creall(cdoble_largo), cimagl(cdoble_largo));

    cflotante    = ccosf((0.2 * M_PI) + (0.5 * M_PI * I));
    cdoble       = ccos((0.4 * M_PI) + (0.3 * M_PI * I));
    cdoble_largo = ccosl((0.6 * M_PI) + (0.1 * M_PI * I));

    printf("El coseno complejo de 0.2pi + 0.5pi*i: %f%+f*i\n", crealf(cflotante), cimagf(cflotante));
    printf("El coseno complejo de 0.4pi + 0.3pi*i: %lf%+lf*i\n", creal(cdoble), cimag(cdoble));
    printf("El coseno complejo de 0.6pi + 0.1pi*i: %Lf%+Lf*i\n", creall(cdoble_largo), cimagl(cdoble_largo));

    cflotante    = ctanf((0.2 * M_PI) + (0.5 * M_PI * I));
    cdoble       = ctan((0.4 * M_PI) + (0.3 * M_PI * I));
    cdoble_largo = ctanl((0.6 * M_PI) + (0.1 * M_PI * I));

    printf("La tangente compleja de 0.2pi + 0.5pi*i: %f%+f*i\n", crealf(cflotante), cimagf(cflotante));
    printf("La tangente compleja de 0.4pi + 0.3pi*i: %lf%+lf*i\n", creal(cdoble), cimag(cdoble));
    printf("La tangente compleja de 0.6pi + 0.1pi*i: %Lf%+Lf*i\n", creall(cdoble_largo), cimagl(cdoble_largo));

    // Funciones trigonométricas inversas complejas
    cflotante    = casinf(1.474858 + 1.861790 * I);
    cdoble       = casin(1.405658 + 0.336314 * I);
    cdoble_largo = casinl(0.998377 - 0.098685 * I);

    printf("El seno inverso complejo de 1.474858+1.861790*i es: %f%+f*i\n", crealf(cflotante), cimagf(cflotante));
    printf("El seno inverso complejo de 1.405658+0.336313*i es: %lf%+lf*i\n", creal(cdoble), cimag(cdoble));
    printf("El seno inverso complejo de 0.998377-0.098685*i es: %Lf%+Lf*i\n", creall(cdoble_largo), cimagl(cdoble_largo));

    cflotante    = cacosf(2.029968 - 1.352670 * I);
    cdoble       = cacos(0.456726 - 1.035069 * I);
    cdoble_largo = cacosl(-0.324392 - 0.303722 * I);

    printf("El coseno inverso complejo de 2.029968-1.352670*i es: %f%+f*i\n", crealf(cflotante), cimagf(cflotante));
    printf("El coseno inverso complejo de 0.456726-1.035069*i es: %lf%+lf*i\n", creal(cdoble), cimag(cdoble));
    printf("El coseno inverso complejo de -0.324392-0.303722*i es: %Lf%+Lf*i\n", creall(cdoble_largo), cimagl(cdoble_largo));

    cflotante    = catanf(0.079914 + 0.970403 * I);
    cdoble       = catan(0.229610 + 1.256718 * I);
    cdoble_largo = catanl(-1.488233 + 1.697621 * I);

    printf("La tangente inversa compleja de 0.079914+0.970403*i es: %f%+f*i\n", crealf(cflotante), cimagf(cflotante));
    printf("La tangente inversa compleja de 0.229610+1.256718*i es: %lf%+lf*i\n", creal(cdoble), cimag(cdoble));
    printf("La tangente inversa compleja de -1.488233+1.697621*i es: %Lf%+Lf*i\n", creall(cdoble_largo), cimagl(cdoble_largo));

    // Funciones trigonométricas hiperbólicas complejas
    cflotante    = csinhf((0.2 * M_PI) + (0.5 * M_PI * I));
    cdoble       = csinh((0.4 * M_PI) + (0.3 * M_PI * I));
    cdoble_largo = csinhl((0.6 * M_PI) + (0.1 * M_PI * I));

    printf("El seno hiperbólico complejo de 0.2pi+0.5pi*i es: %f%+f\n", crealf(cflotante), cimagf(cflotante));
    printf("El seno hiperbólico complejo de 0.4pi+0.3pi*i es: %lf%+lf\n", creal(cdoble), cimag(cdoble));
    printf("El seno hiperbólico complejo de 0.6pi+0.1pi*i es: %Lf%+Lf\n", creall(cdoble_largo), cimagl(cdoble_largo));

    cflotante    = ccoshf((0.2 * M_PI) + (0.5 * M_PI * I));
    cdoble       = ccosh((0.4 * M_PI) + (0.3 * M_PI * I));
    cdoble_largo = ccoshl((0.6 * M_PI) + (0.1 * M_PI * I));

    printf("El coseno hiperbólico complejo de 0.2pi+0.5pi*i es: %f%+f\n", crealf(cflotante), cimagf(cflotante));
    printf("El coseno hiperbólico complejo de 0.4pi+0.3pi*i es: %lf%+lf\n", creal(cdoble), cimag(cdoble));
    printf("El coseno hiperbólico complejo de 0.6pi+0.1pi*i es: %Lf%+Lf\n", creall(cdoble_largo), cimagl(cdoble_largo));

    cflotante    = ctanhf((0.2 * M_PI) + (0.5 * M_PI * I));
    cdoble       = ctanh((0.4 * M_PI) + (0.3 * M_PI * I));
    cdoble_largo = ctanhl((0.6 * M_PI) + (0.1 * M_PI * I));

    printf("La tangente hiperbólica complejo de 0.2pi+0.5pi*i es: %f%+f\n", crealf(cflotante), cimagf(cflotante));
    printf("La tangente hiperbólica complejo de 0.4pi+0.3pi*i es: %lf%+lf\n", creal(cdoble), cimag(cdoble));
    printf("La tangente hiperbólica complejo de 0.6pi+0.1pi*i es: %Lf%+Lf\n", creall(cdoble_largo), cimagl(cdoble_largo));

    // Funciones trigonométricas hiperbólicas inversas complejas
    cflotante    = casinhf((0.2 * M_PI) + (0.5 * M_PI * I));
    cdoble       = casinh((0.4 * M_PI) + (0.3 * M_PI * I));
    cdoble_largo = casinhl((0.6 * M_PI) + (0.1 * M_PI * I));

    printf("El seno hiperbólico inverso complejo de 0.2pi+0.5pi*i es: %f%+f\n", crealf(cflotante), cimagf(cflotante));
    printf("El seno hiperbólico inverso complejo de 0.4pi+0.3pi*i es: %lf%+lf\n", creal(cdoble), cimag(cdoble));
    printf("El seno hiperbólico inverso complejo de 0.6pi+0.1pi*i es: %Lf%+Lf\n", creall(cdoble_largo), cimagl(cdoble_largo));

    cflotante    = cacoshf((0.2 * M_PI) + (0.5 * M_PI * I));
    cdoble       = cacosh((0.4 * M_PI) + (0.3 * M_PI * I));
    cdoble_largo = cacoshl((0.6 * M_PI) + (0.1 * M_PI * I));

    printf("El coseno hiperbólico inverso complejo de 0.2pi+0.5pi*i es: %f%+f\n", crealf(cflotante), cimagf(cflotante));
    printf("El coseno hiperbólico inverso complejo de 0.4pi+0.3pi*i es: %lf%+lf\n", creal(cdoble), cimag(cdoble));
    printf("El coseno hiperbólico inverso complejo de 0.6pi+0.1pi*i es: %Lf%+Lf\n", creall(cdoble_largo), cimagl(cdoble_largo));

    cflotante    = catanhf((0.2 * M_PI) + (0.5 * M_PI * I));
    cdoble       = catanh((0.4 * M_PI) + (0.3 * M_PI * I));
    cdoble_largo = catanhl((0.6 * M_PI) + (0.1 * M_PI * I));

    printf("La tangente hiperbólica inversa complejo de 0.2pi+0.5pi*i es: %f%+f\n", crealf(cflotante), cimagf(cflotante));
    printf("La tangente hiperbólica inversa complejo de 0.4pi+0.3pi*i es: %lf%+lf\n", creal(cdoble), cimag(cdoble));
    printf("La tangente hiperbólica inversa complejo de 0.6pi+0.1pi*i es: %Lf%+Lf\n", creall(cdoble_largo), cimagl(cdoble_largo));

    // Funciones exponenciales complejas
    cflotante    = cexpf(1.0f + 1.0f * I);
    cdoble       = cexp(4.0 + 4.0 * I);
    cdoble_largo = cexpl(-17.9L + 9.0L * I);

    printf("e^(1.0 + 1.0*i) =  %f%+f\n", crealf(cflotante), cimagf(cflotante));
    printf("e^(4.0 + 4.0*i) = %lf%+lf\n", creal(cdoble), cimag(cdoble));
    printf("e^(-17.9 + 9.0*i) = %Lf%+Lf\n", creall(cdoble_largo), cimagl(cdoble_largo));

    // Funciones logarítmicas complejas
    cflotante    = clogf(18.0f + 19.2f * I);
    cdoble       = clog(7.9 + 11.2 * I);
    cdoble_largo = clogl(31.3L - 1.9L * I);

    printf("Logaritmo complejo base e de 18.0+19.2*i = %f%+f\n", crealf(cflotante), cimagf(cflotante));
    printf("Logaritmo complejo base e de 7.9+11.2*i = %lf%+lf\n", creal(cdoble), cimag(cdoble));
    printf("Logaritmo complejo base e de 31.3+1.9*i = %Lf%+Lf\n", creall(cdoble_largo), cimagl(cdoble_largo));

    // Potencia de un número complejo
    cflotante    = cpowf(4.0f + 0.0f * I, 0.0f + 2.0f * I);
    cdoble       = cpow(1.0 + 2.0 * I, 1.0 + 1.0 * I);
    cdoble_largo = cpowl(5.0L * 0.0 * I, 3.0L + 1.0L * I);

    printf("(4.0)^(2.0*i) = %f%+f\n", crealf(cflotante), cimagf(cflotante));
    printf("(1.0+2.0*i)^(1.0+1.0*i) = %lf%+lf\n", creal(cdoble), cimag(cdoble));
    printf("(5.0)^(3.0+1.0*i) = %Lf%+Lf\n", creall(cdoble_largo), cimagl(cdoble_largo));

    // Raíz cuadrada de un número complejo
    flotante    = csqrtf(10.0f - 2.0f * I);
    doble       = csqrt(-5.0);
    doble_largo = csqrtl(9.9L + 12.1L * I);

    printf("Raíz cuadrada compleja de 10.0f-2.0*i: %f%+f\n", crealf(cflotante), cimagf(cflotante));
    printf("Raíz cuadrada compleja de -5.0: %lf%+lf\n", creal(cdoble), cimag(cdoble));
    printf("Raíz cuadrada compleja de 9.9+12.1*i: %Lf%+Lf\n", creall(cdoble_largo), cimagl(cdoble_largo));
}
