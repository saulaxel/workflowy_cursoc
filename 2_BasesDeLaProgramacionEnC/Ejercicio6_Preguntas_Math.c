/*
 * Algunas funciones matemáticas de la biblioteca math
 */

#include <stdio.h>
#include <math.h>
#include <stdbool.h>

int main(void)
{
    // Valor absoluto y modulo para números flotantes

    float flotante          = fabsf(7.7f);     // C99
    double doble            = fabs(-5.5);
    long double doble_largo = fabsl(-5e5L);    // C99

    printf("Absoluto de 7.7:  %f\n", flotante);
    printf("Absoluto de -5.5: %lf\n", doble);
    printf("Absoluto de -5x10^5: %Lf\n", doble_largo);

    flotante    = fmod(8.0f, 7.0f);         // C99
    doble       = fmodf(15.0, 8.5);
    doble_largo = fmodl(891.0L, 123.0L);    // C99

    printf("Módulo de 8.0 y 7.0: %f\n", flotante);
    printf("Módulo de 15.0 y 8.5: %lf\n", doble);
    printf("Módulo de 891.0 y 123.0: %Lf\n", doble_largo);

    // Residuo signado de la división
    flotante    = remainderf(-125.1f, 17.2f);   // C99
    doble       = remainder(234.12, -444.5);    // C99
    doble_largo = remainderl(-1.234, -0.555);   // C99

    printf("Residuo de -125.1 entre 12.7: %f\n", flotante);
    printf("Residuo de 234.12 entre -444.5: %lf\n", doble);
    printf("Residuo de -1.234 entre -0.555: %Lf\n", doble_largo);

    int flotante_q, doble_q, doble_largo_q;
    flotante    = remquof(-125.1f, 17.2f, &flotante_q);     // C99
    doble       = remquo(234.12, -444.5, &doble_q);         // C99
    doble_largo = remquol(-1.234, -0.555, &doble_largo_q);  // C99

    printf("Residuo de -125.1 entre 12.7: %f, últimos dígitos del cociente: %d\n", flotante, flotante_q);
    printf("Residuo de 234.12 entre -444.5: %lf, últimos dígitos del cociente: %d\n", doble, doble_q);
    printf("Residuo de -1.234 entre -0.555: %Lf, últimos dígitos del cociente: %d\n", doble_largo, doble_largo_q);

    // Multiplicación-adición fusionada (fma por sus siglas en inglés)
    flotante    = fmaf(10.0f, 5.0f, 2.0f);  // C99
    doble       = fma(9.0, 8.0, 20.0);      // C99
    doble_largo = fma(50.0L, 70.0L, 125.0L);// C99

    printf("(10.0 * 5.0) + 2.0 = %f\n", flotante);
    printf("(9.0 * 8.0) + 20.0 = %lf\n", doble);
    printf("(50.0 + 70.9) + 125.0 = %Lf\n", doble_largo);

    // Máximo y mínimo de dos valores
    flotante    = fmaxf(5.0f, 7.0f);        // C99
    doble       = fmax(-234.0, 1.1);        // C99
    doble_largo = fmaxl(999.0L, 1000.0L);   // C99

    printf("El mayor de 5.0 y 7.0 = %f\n", flotante);
    printf("El mayor de -234.0 y 1.1 = %lf\n", doble);
    printf("El mayor de 999.0 y 1000.0 = %Lf\n", doble_largo);

    flotante    = fminf(5.0f, 7.0f);        // C99
    doble       = fmin(-234.0, 1.1);        // C99
    doble_largo = fminl(999.0L, 1000.0L);   // C99

    printf("El menor de 5.0 y 7.0 = %f\n", flotante);
    printf("El menor de -234.0 y 1.1 = %lf\n", doble);
    printf("El menor de 999.0 y 1000.0 = %Lf\n", doble_largo);

    // Diferencia positiva (max(0, x - y))
    flotante    = fdimf(10.0f, 5.0f);   // C99
    doble       = fdim(5.0, 10.0);      // C99
    doble_largo = fdiml(732.5, 999.4);  // C99

    printf("La diferencia positiva de 10.0 y 5.0 es: %f\n", flotante);
    printf("La diferencia positiva de 5.0 y 10.0 es: %lf\n", doble);
    printf("La diferencia positiva de 732.5 y 999.4 es: %Lf\n", doble_largo);

    // Funciones exponenciales
    flotante    = expf(2.0f);   // C99
    doble       = exp(5.0);
    doble_largo = expl(10.0L);  // C99

    printf("e^2 = %f\n", flotante);
    printf("e^5 = %lf\n", doble);
    printf("e^10 = %Lf\n", doble_largo);

    flotante    = expm1f(2.0f); // C99
    doble       = expm1(5.0);   // C99
    doble_largo = expm1l(10.0L);// C99

    printf("e^2 - 1 = %f\n", flotante);
    printf("e^5 - 1 = %lf\n", doble);
    printf("e^10 - 1 = %Lf\n", doble_largo);

    flotante    = exp2f(2.0f);  // C99
    doble       = exp2(5.0);    // C99
    doble_largo = exp2l(10.0L); // C99

    printf("2^2 = %f\n", flotante);
    printf("2^5 = %lf\n", doble);
    printf("2^10 = %Lf\n", doble_largo);

    // Funciones logarítmicas
    flotante    = logf(100.f);  // C99
    doble       = log(354.f);
    doble_largo = logl(972.f);  // C99

    printf("logaritmo base e de 100.0: %f\n", flotante);
    printf("logaritmo base e de 354.0: %lf\n", doble);
    printf("logaritmo base e de 972.0: %Lf\n", doble_largo);

    flotante    = log10f(100.f);    // C99
    doble       = log10(354.f);
    doble_largo = log10l(972.f);    // C99

    printf("logaritmo base 10 de 100.0: %f\n", flotante);
    printf("logaritmo base 10 de 354.0: %lf\n", doble);
    printf("logaritmo base 10 de 972.0: %Lf\n", doble_largo);

    flotante    = log2f(100.f); // C99
    doble       = log2(354.f);  // C99
    doble_largo = log2l(972.f); // C99

    printf("logaritmo base 2 de 100.0: %f\n", flotante);
    printf("logaritmo base 2 de 354.0: %lf\n", doble);
    printf("logaritmo base 2 de 972.0: %Lf\n", doble_largo);

    flotante    = log1pf(100.f);    // C99
    doble       = log1p(354.f);     // C99
    doble_largo = log1pl(972.f);    // C99

    printf("logaritmo base 2 de (100.0 + 1): %f\n", flotante);
    printf("logaritmo base 2 de (354.0 + 1): %lf\n", doble);
    printf("logaritmo base 2 de (972.0 + 1): %Lf\n", doble_largo);

    // Potencia de un número
    flotante    = powf(5.5f, 9.9f); // C99
    doble       = pow(-33.0, 6.0);
    doble_largo = powl(123L, 12.0); // C99

    printf("5.5 elevado a 9.9 = %f\n", flotante);
    printf("-33.0 elevado a 6.0 = %lf\n", doble);
    printf("123.0 elevado a 12.0 = %Lf\n", doble_largo);

    // Raíz cuadrada y cúbica de un número
    flotante    = sqrtf(16.0f);     // C99
    doble       = sqrt(625.0);
    doble_largo = sqrtl(10000.0L);  // C99

    printf("raíz cuadrada de 16.0 = %f\n", flotante);
    printf("raíz cuadrada de 625.0 = %lf\n", doble);
    printf("raíz cuadrada de 1000.0L = %Lf\n", doble_largo);

    flotante    = cbrtf(27.0f);         // C99
    doble       = cbrt(62500.0);        // C99
    doble_largo = cbrtl(1000000.0L);    // C99

    printf("raíz cúbica de 27.0 = %f\n", flotante);
    printf("raíz cúbica de 62500.0 = %lf\n", doble);
    printf("raíz cúbica de 1000000.0L = %Lf\n", doble_largo);

    // Hipotenusa de un triángulo
    flotante    = hypotf(2.0f, 3.0f);   // C99
    doble       = hypot(5.0, 12.0);     // C99
    doble_largo = hypotl(65.0L, 72.0L); // C99

    printf("Hipotenusa de un triángulo de 2.0 y 3.0 = %f\n", flotante);
    printf("Hipotenusa de un triángulo de 5.0 y 12.0 = %lf\n", doble);
    printf("Hipotenusa de un triángulo de 65.0 y 72.0 = %Lf\n", doble_largo);

    // Funciones trigonométricas
    flotante    = sinf(0.2 * M_PI); // C99
    doble       = sin(0.4 * M_PI);
    doble_largo = sinl(0.6 * M_PI); // C99

    printf("Seno de 0.2 PI radianes: %f\n", flotante);
    printf("Seno de 0.4 PI radianes: %lf\n", doble);
    printf("Seno de 0.6 PI radianes: %Lf\n", doble_largo);

    flotante    = cosf(0.2 * M_PI); // C99
    doble       = cos(0.4 * M_PI);
    doble_largo = cosl(0.6 * M_PI); // C99

    printf("Coseno de 0.2 PI radianes: %f\n", flotante);
    printf("Coseno de 0.4 PI radianes: %lf\n", doble);
    printf("Coseno de 0.6 PI radianes: %Lf\n", doble_largo);

    flotante    = tanf(0.2 * M_PI); // C99
    doble       = tan(0.4 * M_PI);
    doble_largo = tanl(0.6 * M_PI); // C99

    printf("Tangente de 0.2 PI radianes: %f\n", flotante);
    printf("Tangente de 0.4 PI radianes: %lf\n", doble);
    printf("Tangente de 0.6 PI radianes: %Lf\n", doble_largo);

    // Funciones trigonométricas inversas
    flotante    = asinf(0.2); // C99
    doble       = asin(0.4);
    doble_largo = asinl(0.6); // C99

    printf("Seno inverso de 0.2: %f\n", flotante);
    printf("Seno inverso de 0.4: %lf\n", doble);
    printf("Seno inverso de 0.6: %Lf\n", doble_largo);

    flotante    = acosf(0.2); // C99
    doble       = acos(0.4);
    doble_largo = acosl(0.6); // C99

    printf("Coseno inverso de 0.2: %f\n", flotante);
    printf("Coseno inverso de 0.4: %lf\n", doble);
    printf("Coseno inverso de 0.6: %Lf\n", doble_largo);

    flotante    = atanf(0.2); // C99
    doble       = atan(0.4);
    doble_largo = atanl(0.6); // C99

    printf("Tangente inversa de 0.2: %f\n", flotante);
    printf("Tangente inversa de 0.4: %lf\n", doble);
    printf("Tangente inversa de 0.6: %Lf\n", doble_largo);

    flotante = atan2f(2, 4);  // C99
    doble    = atan2(3, 9);
    doble    = atan2l(5, 25); // C99

    printf("Tangente inversa de 2/4: %f\n", flotante);
    printf("Tangente inversa de 3/9: %lf\n", doble);
    printf("Tangente inversa de 5/25: %Lf\n", doble_largo);

    // Funciones hiperbólicas
    flotante    = sinhf(0.2 * M_PI); // C99
    doble       = sinh(0.4 * M_PI);
    doble_largo = sinhl(0.6 * M_PI); // C99

    printf("Seno hiperbólico de 0.2 PI: %f\n", flotante);
    printf("Seno hiperbólico de 0.4 PI: %lf\n", doble);
    printf("Seno hiperbólico de 0.6 PI: %Lf\n", doble_largo);

    flotante    = coshf(0.2 * M_PI); // C99
    doble       = cosh(0.4 * M_PI);
    doble_largo = coshl(0.6 * M_PI); // C99

    printf("Coseno hiperbólico de 0.2 PI: %f\n", flotante);
    printf("Coseno hiperbólico de 0.4 PI: %lf\n", doble);
    printf("Coseno hiperbólico de 0.6 PI: %Lf\n", doble_largo);

    flotante    = tanhf(0.2 * M_PI); // C99
    doble       = tanh(0.4 * M_PI);
    doble_largo = tanhl(0.6 * M_PI); // C99

    printf("Tangente hiperbólica de 0.2 PI: %f\n", flotante);
    printf("Tangente hiperbólica de 0.4 PI: %lf\n", doble);
    printf("Tangente hiperbólica de 0.6 PI: %Lf\n", doble_largo);

    // Funciones hiperbólicas inversas
    flotante    = asinhf(0.2); // C99
    doble       = asinh(0.4);  // C99
    doble_largo = asinhl(0.6); // C99

    printf("Seno hiperbólico inverso de 0.2: %f\n", flotante);
    printf("Seno hiperbólico inverso de 0.4: %lf\n", doble);
    printf("Seno hiperbólico inverso de 0.6: %Lf\n", doble_largo);

    flotante    = acoshf(0.2); // C99
    doble       = acosh(0.4);  // C99
    doble_largo = acoshl(0.6); // C99

    printf("Coseno hiperbólico inverso de 0.2: %f\n", flotante);
    printf("Coseno hiperbólico inverso de 0.4: %lf\n", doble);
    printf("Coseno hiperbólico inverso de 0.6: %Lf\n", doble_largo);

    flotante    = atanhf(0.2); // C99
    doble       = atanh(0.4);  // C99
    doble_largo = atanhl(0.6); // C99

    printf("Tangente hiperbólica inversa de 0.2: %f\n", flotante);
    printf("Tangente hiperbólica inversa de 0.4: %lf\n", doble);
    printf("Tangente hiperbólica inversa de 0.6: %Lf\n", doble_largo);

    // Función de error gausiana
    flotante    = erff(1.0f);   // C99
    doble       = erf(2.0);     // C99
    doble_largo = erfl(3.0L);   // C99

    printf("Función de error de 1.0: %f\n", flotante);
    printf("Función de error de 2.0: %lf\n", doble);
    printf("Función de error de 3.0: %Lf\n", doble_largo);

    flotante    = erfcf(1.0f);   // C99
    doble       = erfc(2.0);     // C99
    doble_largo = erfcl(3.0L);   // C99

    printf("Función complementaria de error de 1.0: %f\n", flotante);
    printf("Función complementaria de error de 2.0: %lf\n", doble);
    printf("Función complementaria de error de 3.0: %Lf\n", doble_largo);

    // Función gama y su logaritmo
    flotante    = tgammaf(1.0f); // C99
    doble       = tgamma(2.0);   // C99
    doble_largo = tgammal(3.0);  // C99

    printf("Función gamma de 1.0: %f\n", flotante);
    printf("Función gamma de 2.0: %lf\n", doble);
    printf("Función gamma de 3.0: %Lf\n", doble_largo);

    flotante    = lgammaf(1.0f); // C99
    doble       = lgamma(1.0f);  // C99
    doble_largo = lgammal(1.0f); // C99

    printf("Logaritmo base e de la función gamma de 1.0: %f\n", flotante);
    printf("Logaritmo base e de la función gamma de 2.0: %lf\n", doble);
    printf("Logaritmo base e de la función gamma de 3.0: %Lf\n", doble_largo);

    // Redondeos y truncamientos de valores
    flotante    = ceilf(1.3);   // C99
    doble       = ceil(1.5);
    doble_largo = ceill(1.7);   // C99

    printf("1.3 truncando hacia abajo: %f\n", flotante);
    printf("1.5 truncando hacia abajo: %lf\n", doble);
    printf("1.7 truncando hacia abajo: %Lf\n", doble_largo);

    flotante    = floorf(1.3);   // C99
    doble       = floor(1.5);
    doble_largo = floorl(1.7);   // C99

    printf("1.3 truncando hacia arriba: %f\n", flotante);
    printf("1.5 truncando hacia arriba: %lf\n", doble);
    printf("1.7 truncando hacia arriba: %Lf\n", doble_largo);

    flotante    = truncf(1.3);   // C99
    doble       = trunc(1.5);    // C99
    doble_largo = truncl(1.7);   // C99

    printf("1.3 truncando hacia cero: %f\n", flotante);
    printf("1.5 truncando hacia cero: %lf\n", doble);
    printf("1.7 truncando hacia cero: %Lf\n", doble_largo);

    flotante    = roundf(1.3);   // C99
    doble       = round(1.5);    // C99
    doble_largo = roundl(1.7);   // C99

    printf("1.3 redondeado alejándose de cero: %f\n", flotante);
    printf("1.5 redondeado alejándose de cero: %lf\n", doble);
    printf("1.7 redondeado alejándose de cero: %Lf\n", doble_largo);

    long l_f, l_lf, l_Lf;
    l_f  = lroundf(1.3f);   // C99
    l_lf = lround(1.5);     // C99
    l_Lf = lroundl(1.7L);   // C99

    printf("1.3 redondeado alejándose de cero y convertido a long: %ld\n", l_f);
    printf("1.5 redondeado alejándose de cero y convertido a long: %ld\n", l_lf);
    printf("1.7 redondeado alejándose de cero y convertido a long: %ld\n", l_Lf);

    long long ll_f, ll_lf, ll_Lf;
    ll_f  = llroundf(1.3f);     // C99
    ll_lf = llround(1.5);       // C99
    ll_Lf = llroundl(1.7L);     // C99

    printf("1.3 redondeado alejándose de cero y convertido a long long: %lld\n", ll_f);
    printf("1.5 redondeado alejándose de cero y convertido a long long: %lld\n", ll_lf);
    printf("1.7 redondeado alejándose de cero y convertido a long long: %lld\n", ll_Lf);

    flotante    = nearbyintf(1.3f);     // C99
    doble       = nearbyintf(1.5);      // C99
    doble_largo = nearbyintf(1.7l);     // C99

    printf("1.3 redondeado según el modo de redondeo actual: %f\n", flotante);
    printf("1.5 redondeado según el modo de redondeo actual: %lf\n", doble);
    printf("1.7 redondeado según el modo de redondeo actual: %Lf\n", doble_largo);

    flotante    = rintf(1.3f);  // C99
    doble       = rint(1.5);    // C99
    doble_largo = rintl(1.7l);  // C99

    printf("1.3 redondeado según el modo de redondeo actual y con posibilidad de causar excepciones: %f\n", flotante);
    printf("1.5 redondeado según el modo de redondeo actual y con posibilidad de causar excepciones: %lf\n", doble);
    printf("1.7 redondeado según el modo de redondeo actual y con posibilidad de causar excepciones: %Lf\n", doble_largo);

    l_f  = lrintf(1.3f);    // C99
    l_lf = lrint(1.5);      // C99
    l_Lf = lrintl(1.7l);    // C99

    printf("1.3 redondeado según el modo de redondeo actual y con posibilidad de causar excepciones y convertido a long: %f\n", flotante);
    printf("1.5 redondeado según el modo de redondeo actual y con posibilidad de causar excepciones y convertido a long: %lf\n", doble);
    printf("1.7 redondeado según el modo de redondeo actual y con posibilidad de causar excepciones y convertido a long: %Lf\n", doble_largo);

    ll_f  = llrintf(1.3f);  // C99
    ll_lf = llrint(1.5);    // C99
    ll_Lf = llrintl(1.7l);  // C99

    printf("1.3 redondeado según el modo de redondeo actual y con posibilidad de causar excepciones y convertido a long long: %f\n", flotante);
    printf("1.5 redondeado según el modo de redondeo actual y con posibilidad de causar excepciones y convertido a long long: %lf\n", doble);
    printf("1.7 redondeado según el modo de redondeo actual y con posibilidad de causar excepciones y convertido a long long: %Lf\n", doble_largo);

    // Multiplicar un número por 2^n
    flotante    = ldexpf(5.0f, 4.0f);       // C99
    doble       = ldexp(10.0, 6.0);         // C99
    doble_largo = ldexpl(100.0L, 10.0L);    // C99

    printf("5.0 * 2.0^4.0 = %f\n", flotante);
    printf("10.0 * 2.0^6.0 = %lf\n", doble);
    printf("100.0 * 2.0^10.0 = %Lf\n", doble_largo);

    // Separar un número en parte entera y parte decimal
    float iflotante; double idoble; long double idoble_largo;
    flotante    = modff(10.5, &iflotante);     // C99
    doble       = modf(12.1, &idoble);
    doble_largo = modfl(15.3, &idoble_largo);  // C99

    printf("La parte entera de 10.5 es %f y su parte decimal es %f\n", iflotante, flotante);
    printf("La parte entera de 12.1 es %lf y su parte decimal es %lf\n", idoble, doble);
    printf("La parte entera de 15.3 es %Lf y su parte decimal es %Lf\n", idoble_largo, doble_largo);

    // Calcular un NumeroA por FLT_RADI^NumeroB
    flotante    = scalbnf(1.0f, 2); // C99
    doble       = scalbn(2.0, 4);   // C99
    doble_largo = scalbnl(3.0, 6);  // C99

    printf("1.0 * FLT_RADIX^2 = %f\n", flotante);
    printf("2.0 * FLT_RADIX^4 = %lf\n", doble);
    printf("3.0 * FLT_RADIX^6 = %Lf\n", doble_largo);

    flotante    = scalblnf(1.0f, 2L); // C99
    doble       = scalbln(2.0, 4L);   // C99
    doble_largo = scalblnl(3.0, 6L);  // C99

    printf("1.0 * FLT_RADIX^2 = %f\n", flotante);
    printf("2.0 * FLT_RADIX^4 = %lf\n", doble);
    printf("3.0 * FLT_RADIX^6 = %Lf\n", doble_largo);
}
