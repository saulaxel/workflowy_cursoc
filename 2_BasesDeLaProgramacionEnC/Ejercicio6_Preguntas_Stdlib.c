/*
 * Algunas funciones matemáticas de stdlib
 */

// ##### Preprocesador #####
#include <stdio.h>      // para printf(), scanf(), puts()
#include <stdlib.h>     // para EXIT_FAILURE, EXIT_SUCCESS

int main(void)
{
    // Valor absoluto
    printf("El valor absoluto de -5 es: %d\n", abs(-5));
    printf("El valor absoluto de 777 es: %ld\n", labs(-777L));
    printf("El valor absoluto de -25234 es: %lld\n", llabs(-25234LL)); // C99

    // Divisiones enteras
    div_t division_int   = div(5, 2);
    ldiv_t division_long = ldiv(7L, 3L);
    lldiv_t division_long_long = lldiv(155LL, 43LL); // C99
    printf("Al dividir 5 entre 2 obtienes %d como cociente y %d como residuo\n",
            division_int.quot, division_int.rem);
    printf("Al dividir 7 entre 3 obtienes %ld como cociente y %ld como residuo\n",
            division_long.quot, division_long.rem);
    printf("Al dividir 155 entre 43 obtienes %lld como cociente y %lld como residuo\n",
            division_long_long.quot, division_long_long.rem);

    // Números pseudo-aleatorios
    srand(7); // Semilla cualquiera
    printf("5 números pseudo-aleatorios: %d %d %d %d %d\n",
            rand(), rand(), rand(), rand(), rand());
}
