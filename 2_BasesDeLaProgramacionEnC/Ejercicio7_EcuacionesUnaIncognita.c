/*
 * Resolver un sistema de ecuaciones lineales de una
 * sola incógnita.
 */

#include <stdio.h>
#include <stdlib.h>

int main(void)
{

    float a, b;      // Coeficientes de la ecuación
    char nombre_var; // Nombre de la variable

    puts("\t ___                            ___   \n"
         "\t||  |   ___         ___        ||  |  \n"
         "\t|| _|__/  _\\_______/  _\\_______|| _|\n"
         "\t||(___(  (________(  (_________||((_) \n"
         "\t||  |  \\___/       \\___/       ||  |\n"
         "\t||  |         ___              ||  |  \n"
         "\t|| _|________/  _\\_____________|| _| \n"
         "\t||(_________(  (_______________||((_) \n"
         "\t||  |        \\___/             ||  | \n"
         "\t||  |                          ||  |  \n"
         "\t||  |      Calculadora de      ||  |  \n"
         "\t||  |        ecuaciones        ||  |  \n");

    // Leyendo datos
    printf(" Ingrese una ecuación de la forma aX=b (sin espacios) donde:\n"
           "   a y b son números y X es una variable de una sola letra  \n"
           " > ");

    int datos_leidos = scanf("%f%c=%f", &a, &nombre_var, &b);

    if (datos_leidos != 3)
    {
        puts("Los datos ingresados son incorrectos. Cerrando el programa.");
        return EXIT_FAILURE;
    }

    // Calculando la respuesta y mostrándola
    float respuesta = b / a;
    printf("%g%c=%g   --->   %c=%g\n", a, nombre_var, b,
                                       nombre_var, respuesta);

    return EXIT_SUCCESS;
}
