/*
 * Hay una serie de vídeos el Workflowy que contienen problemas aritméticos.
 * Este programa pasa las ecuaciones mostradas a un programa y muestra los resultados.
 */

#include <stdio.h> // Para usar printf()

// Función auxiliar: mostrar_resultado
// Sirve para ahorrarse multiples llamadas a printf()
void mostrar_resultado(int num_ejercicio, float resultado)
{
    // %g imprime un flotante usando solo la cantidad justa de dígitos
    printf("El resultado del ejercicio %d es %g\n", num_ejercicio, resultado);
}

int main(void)
{
    float respuesta;
    // ##### Problema 1: Sumas consecutivas #####
    respuesta = 1000.0 + 40.0 + 1000.0 + 30.0 + 1000.0 + 20.0 + 1000.0 + 10.0;
    // Nótese como se añade un .0 a los números (se puede omitir el 0, con un punto basta)
    // para asegurar que toda la operación se realize con flotantes y por tanto sea exacta
    // En teoria no es necesario que todos los operandos sean flotantes dada la "conversión automática"
    // que pasa cuando se hacen operaciones con tipos distintos, sin embargo es recomendable
    // que se usen siempre ".0" para evitar errores.
    mostrar_resultado(1, respuesta);


    // ##### Problema 2: Tres y Tres y Tres #####
    respuesta = 3. + 3. * 3. - 3. + 3.; // Simplemente poner un punto es lo mismo que añadir .0
    mostrar_resultado(2, respuesta);

    // ##### Problema 3: Mitad de 2 + 2 #####
    // Dado que el lenguaje humano es ambiguo, este
    // problema puede tener dos respuestas:
    respuesta = (2.0 / 2.0) + 2.0;
    mostrar_resultado(3, respuesta); // Se muestra la respuesta 1 del problema 3

    respuesta = (2.0 + 2.0) / 2.0;
    mostrar_resultado(3, respuesta); // Se muestra la respuesta 2 del problema 3

    // ##### Problema 4: División entre 1/2 #####
    respuesta = 40.0 / (1.0 / 2.0) + 15; // Nótese que los números fraccionarios requieren paréntesis
    mostrar_resultado(4, respuesta);

    // ##### Problema 5: Julio profe !! #####
    respuesta = (2.0 / 3.0) / (5.0 / ((1.0 / 2.0) + 1) - 3 * ((1.0 / 2.0) - (1.0 / 4.0)));
    mostrar_resultado(5, respuesta);

    // ##### Problema 6: 6 / 2 (1 + 2) #####
    // Una multiplicación implícita como 2(1 + 2) se tiene que volver explícita como 2 * (1 + 2)
    respuesta = 6.0 / 2.0 * 3.0;
    mostrar_resultado(6, respuesta);

    // ##### Problema 7: 9 - 3 / (1/3) + 1 #####
    respuesta = 9.0 - 3 / (1.0 / 3.0) + 1;
    mostrar_resultado(7, respuesta);
}