/*
 * Usar sentencias if para determinar cual de los números
 * ingresados es mayor y cual menor.
 */

#include <stdio.h>
#include <stdbool.h>    // para usar el tipo bool

int main(void)
{
    int a, b; // Los números a comparar

    printf("Comprobar el funcionamiento del los operadores relacionales\n\n");

    printf("Ingrese dos números: ");
    scanf("%d%d", &a, &b);

    if (a > b)
    {
        printf("%d es mayor que %d\n", a, b);
    }
    else if (b > a)
    {
        printf("%d es mayor que %d\n", b, a);
    }
    else
    {
        printf("Ambos son iguales\n");
    }

    return 0;
}
