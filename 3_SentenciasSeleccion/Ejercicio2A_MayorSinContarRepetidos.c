/*
 * Recibir tres números del teclado e imprimir el mayor. No
 * tomar en cuenta si hay números repetidos.
 */

#include <stdio.h>
#include <stdbool.h>

int main(void)
{
    int num1, num2, num3;

    printf("Ingresa tres valores para comprobar cual es mayor: ");
    scanf("%d%d%d", &num1, &num2, &num3);

    int mayor;

    if ((num1 >= num2) && (num1 >= num3))
    {
        mayor = num1;
    }
    else if ((num2 >= num1) && (num2 >= num3))
    {
        mayor = num2;
    }
    else
    {
        mayor = num3;
    }

    printf("El mayor es: %d\n", mayor);
}
