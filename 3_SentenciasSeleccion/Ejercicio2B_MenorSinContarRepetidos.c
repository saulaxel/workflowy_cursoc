/*
 * Recibir tres números del teclado e imprimir el menor. No
 * tomar en cuenta si hay números repetidos.
 */

#include <stdio.h>
#include <stdbool.h>

int main(void)
{
    int num1, num2, num3;

    printf("Ingresa tres valores para comprobar cual es menor: ");
    scanf("%d%d%d", &num1, &num2, &num3);

    int menor;

    if ((num1 <= num2) && (num1 <= num3))
    {
        menor = num1;
    }
    else if ((num2 <= num1) && (num2 <= num3))
    {
        menor = num2;
    }
    else
    {
        menor = num3;
    }

    printf("El menor es: %d\n", menor);
}

