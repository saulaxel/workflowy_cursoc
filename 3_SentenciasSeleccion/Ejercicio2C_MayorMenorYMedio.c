/*
 * Recibir tres números del teclado e indicar:
 * - Si todos los números son diferentes, debe indicarse cual es el mayor,
 *   el de en medio y el menor.
 * - Si son todos iguales, indicar que los tres números son el mismo.
 * - Si hay dos iguales y uno diferente, indicar cuales se repiten y cual no,
 *   así como si los repetidos son mayores o menores al número único.
 */

#include <stdio.h>
#include <stdbool.h>

const char * mayor_o_menor(int a, int b)
{
    if (a > b)
    {
        return "mayor";
    }
    else
    {
        return "menor";
    }
}

int main(void)
{
    int num1, num2, num3;

    printf("Ingresa tres valores para comprobar cual es menor: ");
    scanf("%d%d%d", &num1, &num2, &num3);

    if (num1 == num2 && num2 == num3)
    {
        printf("Los tres números son %d\n", num1);
    }
    else if (num1 != num2 && num2 != num3 && num1 != num3)
    {
        int mayor, medio, menor;
        if (num1 > num2 && num1 > num3)
        {
            mayor = num1;
            if (num2 > num3)
            {
                mayor = num2, menor = num3;
            }
            else
            {
                mayor = num3, menor = num2;
            }
        }

        printf("Mayor: %d; En medio: %d; Menor: %d\n", mayor, medio, menor);
    }
    else
    {
        if (num1 == num2)
        {
            printf("El número %d se repite\n", num1);
            printf("Dicho número es %s que %d\n", mayor_o_menor(num1, num3), num3);
        }
        else if (num2 == num3)
        {
            printf("El número %d se repite\n", num2);
            printf("Dicho número es %s que %d\n", mayor_o_menor(num2, num1), num1);
        }
        else
        {
            printf("El número %d se repite\n", num2);
            printf("Dicho número es %s que %d\n", mayor_o_menor(num1, num2), num2);
        }
    }
}
