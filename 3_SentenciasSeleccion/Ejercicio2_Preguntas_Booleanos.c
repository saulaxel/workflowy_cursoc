/*
 * Mostrando algunas propiedades de los booleanos
 */

#include <stdio.h>
#include <stdbool.h> // Alias para los booleanos

int main(void)
{
    bool a = true;

    printf("true es igual a %d\n", a);

    a = false;

    printf("false es igual a %d\n", a);

    a = -123;

    printf("-123 convertido a bool es %d\n", a);

    a = 0.01;

    printf("0.01 convertido a bool es %d\n", a);
}
