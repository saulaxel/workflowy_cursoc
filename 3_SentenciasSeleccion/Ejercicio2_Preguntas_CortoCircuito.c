/*
 * Demostración de como afecta el "corto circuito"
 * al hacer operaciones lógicas.
 */

#include <stdio.h>

int main(void)
{
    int a = 1;
    int b = 1;
    int c = ++a || b++;
    int d = b-- && --a;

    printf("%d %d %d %d\n", a, b, c, d);
}
