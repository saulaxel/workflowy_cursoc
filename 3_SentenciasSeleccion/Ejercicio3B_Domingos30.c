/*
 * Programa para aconsejar al usuario que lleve paraguas o abrigo al
 * salir basados en el clima y en el día.
 */

#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <stdlib.h>
#include <ctype.h>

void limpiar_pantalla(void)
{
#ifdef _WIN32
    system("cls");
#else
    system("clear");
#endif
}

void hablar_asistente(char mensaje[])
{
    int largo = strlen(mensaje);

    int relleno_izquierda = 24 - largo / 2 - largo % 2;
    int relleno_derecha   = 24 - largo / 2;

    limpiar_pantalla();
    printf("< %*s%s%*s >\n"
           " ------------------------------------------------ \n"
           "                      \\                          \n"
           "                        \\                        \n"
           "                       .--.                       \n"
           "                      |o_o |                      \n"
           "                      |:_/ |                      \n"
           "                     //   \\ \\                   \n"
           "                    (|     | )                    \n"
           "                    /'\\_   _/`\\                 \n"
           "                    \\___)=(___/                  \n",
           relleno_izquierda, "",
           mensaje,
           relleno_derecha, "");

}

int main(void)
{
    float temperatura;   // En celcius
    char opcion;
    bool esta_lloviendo;
    char dia_semana[32];
    int dia_del_mes;

    hablar_asistente("Hola, Human@. ¿Qué temperatura hace?");
    scanf("%f", &temperatura);

    hablar_asistente("Mmmm, interesante. Y dime, ¿está lloviendo?");
    puts("\n"
         "\t a) Si, lo está\n"
         "\t b) No, no llueve.\n");

    scanf("\n%c", &opcion);
    esta_lloviendo = (opcion == 'a');

    hablar_asistente("¿Qué día de la semana es (nombre completo)?");
    scanf("\n%s", dia_semana);

    hablar_asistente("Y finalmente, ¿Qué día del mes es?");
    scanf("%d", &dia_del_mes);

    for (int i = 0; dia_semana[i]; i++)
    {
        // transformar todo a minúscula
        dia_semana[i] = tolower(dia_semana[i]);
    }

    if (dia_del_mes == 30 || strcmp(dia_semana, "domingo") == 0)
    {
        hablar_asistente("Hoy no te preocupes, disfruta");
        return 0;
    }

    char * mensaje;

    if (temperatura < 18.0)
    {
        if (esta_lloviendo)
        {
            mensaje = "Uyy que tiempo. Lleva contigo abrigo y paraguas";
        }
        else
        {
            mensaje = "Te recomiendo llevar un abrigo";
        }
    }
    else if (esta_lloviendo)
    {
        mensaje = "Lleva paraguas para protegerte de la lluvia";
    }
    else if (temperatura > 35.0)
    {
        mensaje = "Deberías llevar paraguas para el sol";
    }
    else
    {
        mensaje = "No es necesario que lleves nada hoy";
    }

    hablar_asistente(mensaje);
}
