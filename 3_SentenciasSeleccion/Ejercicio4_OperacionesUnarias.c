/*
 * Calculadora que permite elegir entre elevar al cuadrado,
 * sacar raíz cuadrada, logaritmo base 10 y valor absoluto de
 * un número.
 */

#include <stdio.h>
#include <math.h>   // Para las operaciones matemáticas

void limpiar_buffer(void)
{
    scanf("%*[^\n]"); // Limpia hasta el salto de línea pero ignorando el mismo
    scanf("%*c");     // Se lleva también el salto de línea
}

int main(void)
{
    int opcion = -1;

    do {

        puts("¿Qué deseas hacer?\n"
             " 1) Elevar al cuadrado\n"
             " 2) Sacar la raíz cuadrada\n"
             " 3) Sacar logaritmo base 10\n"
             " 4) Sacar valor absoluto\n"
             " 5) Salir\n");

        int valores_leidos = scanf("%d", &opcion);

        if (valores_leidos != 1)
        {
            printf("Valor incorrecto\n");
            limpiar_buffer();
        }

        if (opcion < 1 || opcion > 5)
        {
            printf("La opción debe estar entre 1 y 5\n");
            continue;
        }

        float operando;

        if (opcion != 5)
        {
            valores_leidos = scanf("%f", &operando);

            if (valores_leidos != 1)
            {
                printf("El número ingresado no es válido\n");
                limpiar_buffer();
                continue;
            }
        }

        switch (opcion)
        {
          case 1:
            printf("%f al cuadrado es %f\n", operando, operando * operando);
            break;

          case 2:
            printf("La raíz de %f es %f\n", operando, sqrt(operando));
            break;

          case 3:
            printf("El logaritmo base 10 de %f es %f\n", operando, log10(operando));
            break;

          case 4:
            printf("El valor absoluto de %f es %f\n", operando, fabs(operando));
            break;
        }
    } while (opcion != 5);
}
