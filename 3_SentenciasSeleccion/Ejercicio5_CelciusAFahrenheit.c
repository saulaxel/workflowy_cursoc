/*
 * Transformar celcius a farenheit y viceversa
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

void validar(bool condicion, char mensaje[])
{
    if (condicion == false)
    {
        puts(mensaje);
        exit(EXIT_FAILURE); // Sale del programa si la condición falla
    }
}

int main(void)
{
    puts("\t - Conversor de temperaturas - \n");

    int opcion;
    puts("¿Qué operación quiere realizar?\n"
         " 1) Convertir grados Celcius a Fahrenheit\n"
         " 2) Convertir grados Fahrenheit a Celcius\n");

    validar(scanf("%d", &opcion) == 1, "Se tiene que ingresar un número");
    validar(opcion == 1 || opcion == 2, "La opción es inválida");

    if (opcion == 1)
    {
        float c;
        printf("Ingrese la cantidad de grados Celcius\n");

        validar(scanf("%f", &c) == 1, "La cantidad es incorrecta");
    }
    else if (opcion == 2)
    {
        float f;
        printf("Ingrese la cantidad de grados Fahrenheit\n");

        validar(scanf("%f", &f) == 1, "La cantidad es incorrecta");
    }

    return EXIT_SUCCESS; // El programa terminó con éxito
}
