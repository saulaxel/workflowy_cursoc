/*
 * Programa que resuelve sistemas de ecuaciones
 * de 1 x 1 o de 2 x 2 dependiendo de la elección del usuario.
 */

#include <stdio.h>
#include <math.h>   // para la función isfinite()
#include <string.h> // para strlen()

// Prototipos de función
void resolverEcuacion1x1(void);
void resolverEcuacion2x2(void);
void quitarEspaciosACadena(char cadena[]);

int main(void)
{
    puts("\t ___                            ___   \n"
         "\t||  |   ___         ___        ||  |  \n"
         "\t|| _|__/  _\\_______/  _\\_______|| _|\n"
         "\t||(___(  (________(  (_________||((_) \n"
         "\t||  |  \\___/       \\___/       ||  |\n"
         "\t||  |         ___              ||  |  \n"
         "\t|| _|________/  _\\_____________|| _| \n"
         "\t||(_________(  (_______________||((_) \n"
         "\t||  |        \\___/             ||  | \n"
         "\t||  |                          ||  |  \n"
         "\t||  |      Calculadora de      ||  |  \n"
         "\t||  |        ecuaciones        ||  |  \n");

    char opcion;
    do {
        printf("Elige que tipo de ecuación quieres resolver:\n"
               " a) Sistema de ecuaciones de 1 x 1\n"
               " b) Sistema de ecuaciones de 2 x 2\n");

        scanf("\n%c", &opcion);

    } while (opcion != 'a' && opcion != 'b');

    // Leyendo datos
    if (opcion == 'a')
    {
        resolverEcuacion1x1();
    }
    else
    {
        resolverEcuacion2x2();
    }
}

void resolverEcuacion1x1(void)
{
    float a, b;      // Coeficientes de la ecuación
    char nombre_var; // Nombre de la variable

    printf(" Ingrese una ecuación de la forma aX=b donde:\n"
           " a y b son números y X es una variable de una sola letra\n"
           " > ");

    char ecuacion[128];
    fgets(ecuacion, 128, stdin);
    quitarEspaciosACadena(ecuacion);

    int datos_leidos = sscanf(ecuacion, "%f%c=%f", &a, &nombre_var, &b);

    if (datos_leidos != 3)
    {
        puts("Los datos ingresados son incorrectos. Cerrando el programa.");
        return;
    }

    // Calculando la respuesta y mostrándola
    float respuesta = b / a;

    if (isfinite(respuesta))
    {
        printf("%g%c=%g   --->   %c=%g\n", a, nombre_var, b,
                                           nombre_var, respuesta);
    }
    else
    {
        printf("No se pudo sacar respuesta de la ecuación\n");
    }
}

void resolverEcuacion2x2(void)
{
    float a, b, c, d, e, f;        // Coeficientes de la ecuación
    char nombre_var1, nombre_var2; // Nombres de las dos variables

    printf(" Se resuelven sistemas de la forma:\n"
           "    aX+bY=c\n"
           "    dY+eY=f\n"
           " Donde a, b, c, d, e y f son números y X e Y son variables de una letra\n");

    char ecuacion[128];

    printf("Ingrese la primera ecuación: ");
    fgets(ecuacion, 128, stdin);

    printf("Ingrese la segunda ecuación: ");
    fgets(ecuacion, 128, stdin);
}

void quitarEspaciosACadena(char cadena[])
{
    int longitud_cadena = strlen(cadena);

    int pos = 0;
    for (int i = 0; i < longitud_cadena; i++)
    {
        if (cadena[i] != '\t' && cadena[i] != ' ')
        {
            cadena[pos++] = cadena[i];
        }
    }
}
