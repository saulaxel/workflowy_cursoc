/*
 * Programa para resolver ecuaciones cuadráticas
 */

#include <stdio.h>
#include <stdlib.h>     // para la función exit() y las constantes como EXIT_FAILURE
#include <complex.h>    // Para hacer operaciones que envuelvan números complejos
#include <stdbool.h>    // alias para los booleanos

void validar(bool condicion)
{
    if (condicion == false)
    {
        puts("El dato es erróneo");
        exit(EXIT_FAILURE);
    }
}

int main(void)
{
    // Ax^2 + Bx + C = 0
    float A, B, C;

    puts("\t > Resolver ecuaciones cuadráticas <");

    printf("Con la ecuación en forma Ax^2 + Bx + C, ingrese:\n");

    printf("Coeficiente A: ");
    validar(scanf("%f", &A));

    printf("Coeficiente B: ");
    validar(scanf("%f", &B));

    printf("Coeficiente C: ");
    validar(scanf("%f", &C));


}
