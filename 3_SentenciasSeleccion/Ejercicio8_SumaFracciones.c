/*
 * Realizar la suma de dos números fraccionarios
 * y mostrar el resultado como fracción. No es necesario
 * simplificar la fracción.
 */

#include <stdio.h>
#include <stdbool.h>

int main(void)
{
    int numerador1, denominador1; // Para la fracción 1
    int numerador2, denominador2; // Para la fracción 2

    // Leyendo los datos
    puts("\t |      Calculador de fracciones      |\n"
         "\t |                          _______   |\n"
         "\t |       ____   oo    k    |/      |  |\n"
         "\t |      / pi    __ (x)     |      (_) |\n"
         "\t |     / ---  + \\  ---  =  |      \\|/ |\n"
         "\t |   \\/   2     /_  k!     |       |  |\n"
         "\t |              k=1        |      / \\ |\n"
         "\t |                                    |\n"
         "\t <------------------------------------>\n");


    bool datos_validos;

    do {
        printf("Ingrese la fracción 1 en formato a/b (sin espacios):\n");
        datos_validos = (scanf("%d/%d", &numerador1, &denominador1) == 2);

        if (!datos_validos)
        {
            puts("Error de formato en la fracción");
            scanf("%*[^\n]"); // Se elimina la basura excepto el salto de línea
            scanf("%*c");     // Se elimina el salto de línea
        }
    } while (!datos_validos);


    do {
        printf("Ingrese la fracción 2 en formato a/b (sin espacios):\n");
        datos_validos = (scanf("%d/%d", &numerador2, &denominador2) == 2);

        if (!datos_validos)
        {
            puts("Error de formato en la fracción");
            scanf("%*[^\n]"); // Se elimina la basura excepto el salto de línea
            scanf("%*c");     // Se elimina el salto de línea
        }
    } while (!datos_validos);

    // Calcular el resultado
    int numerador_res, denominador_res;
}
