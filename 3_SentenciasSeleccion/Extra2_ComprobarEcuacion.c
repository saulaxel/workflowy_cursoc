/*
 * Comprobar que una serie de números sea
 * solución de un sistema de ecuaciones dado.
 */

#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>

int leer_digito(char nombre_var)
{
    int d;
    bool dato_valido = false;

    printf("Ingresa el dígito correspondiente a %c:\n", nombre_var);
    do {

        printf("> ");

        // Se lee el dato y se revisan todas sus condiciones
        dato_valido = (scanf("%d", &d) == 1) && (d >= 1) && (d <= 9);

        if (!dato_valido)
        {
            printf("Te dije que ingresaras un dígito >:v\n");

            scanf("%*[^\n]"); // Borra la basura hasta el salto de línea
            scanf("%*c");     // Borra el salto de línea
        }
    } while (!dato_valido);

    return d;
}

int main(void)
{
    int A, B, C, D;

    // Leyendo los datos
    A = leer_digito('A');
    B = leer_digito('B');
    C = leer_digito('C');
    D = leer_digito('D');

    // Comprobando que sea solución de la ecuación
    // A + C = D
    // A * B = C
    // C - B = B
    // A * 4 = D

    /* Inserte código aquí */

}
