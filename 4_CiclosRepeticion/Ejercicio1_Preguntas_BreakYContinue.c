/*
 * Demostración de break y continue
 * dentro de ciclos.
 */

#include <stdio.h>

int main(void)
{
    puts("Primer ciclo:");

    for (int i = 1; i <= 10; i++)
    {
        if (i == 5)
        {
            break;
        }

        printf("Voy en el número %d\n", i);
    }

    puts("\nSegundo ciclo:");
    for (int i = 1; i <= 10; i++)
    {
        if (i == 5)
        {
            continue;
        }

        printf("Voy en el número %d\n", i);
    }
}
