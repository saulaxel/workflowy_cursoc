/*
 * Un programa que hace una pregunta de sí o no
 * y obliga al usuario a decir que sí.
 */

#include <stdio.h>

int main(void)
{
    int opcion = 0;

    printf("¿Quiere donar a la caridad?\n"
           " 1) Sí\n"
           " 2) No\n");

    if (scanf("%d", &opcion) != 1)
    {
        // Si se ingresó basura hay que limpiarla
        scanf("%*[^\n]"); // Se quita todo menos el salto de línea
        scanf("%*c");     // Se quita el salto de línea
    }
}
