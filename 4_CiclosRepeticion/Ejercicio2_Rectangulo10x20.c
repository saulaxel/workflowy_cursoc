/*
 * Imprimir en pantalla un rectángulo de 10 x 20
 * formado de asteriscos.
 */

#include <stdio.h>

int main(void)
{
    puts("Forma 1:\n");

    printf("**********\n");
    printf("**********\n");
    printf("**********\n");
    printf("**********\n");
    printf("**********\n");
    printf("**********\n");
    printf("**********\n");
    printf("**********\n");
    printf("**********\n");
    printf("**********\n");
    printf("**********\n");
    printf("**********\n");
    printf("**********\n");
    printf("**********\n");
    printf("**********\n");
    printf("**********\n");
    printf("**********\n");
    printf("**********\n");
    printf("**********\n");
    printf("**********\n");

    puts("\nForma 2:\n");

    printf("********************\n");
    printf("********************\n");
    printf("********************\n");
    printf("********************\n");
    printf("********************\n");
    printf("********************\n");
    printf("********************\n");
    printf("********************\n");
    printf("********************\n");
    printf("********************\n");
}
