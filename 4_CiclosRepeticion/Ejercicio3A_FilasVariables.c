/*
 * Imprimir un rectángulo en pantalla
 * con 20 columnas y filas variables.
 */

#include <stdio.h>
#include <stdbool.h>

#define COLUMNAS 20

int main(void)
{
    int filas;

    printf("Ingrese las filas del rectángulo: ");
    bool valido = (scanf("%d", &filas) == 1) && (filas > 0);

    if (!valido)
    {
        puts("El número no es válido");
    }

    /* Imprimir el rectángulo */
}
