/*
 * Imprimir un rectángulo en pantalla
 * con 10 filas y columnas variables.
 */

#include <stdio.h>
#include <stdbool.h>

#define FILAS 10

int main(void)
{
    int columnas;

    printf("Ingrese las columnas del rectángulo: ");
    bool valido = (scanf("%d", &columnas) == 1) && (columnas > 0);

    if (!valido)
    {
        puts("El número no es válido");
    }

    /* Imprimir el rectángulo */
}

