/*
 * Imprimir un rectángulo en pantalla
 * con filas y columnas variables.
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <ctype.h>  // Funciones de clasificación de caracteres como isgraph()

int main(void)
{
    int filas, columnas;
    char caracter;

    printf("Ingrese las filas y las columnas del rectángulo: ");
    bool valido = (scanf("%d%d", &filas, &columnas) == 2);

    if (!valido)
    {
        puts("Alguno de los números no es válido");
        return EXIT_FAILURE;
    }

    if (filas < 0 || columnas < 0)
    {
        puts("No existen las longitudes menores a 0");
        return EXIT_FAILURE;
    }


    printf("Ahora ingrese el carácter a usar: \n");
    scanf("\n%c", &caracter); // Se quita el salto de línea y se lee el carácter

    if (!isgraph(caracter)) // isgraph comprueba si el carácter es visible
    {
        puts("El carácter ingresado no es visible, elija otro");
        return EXIT_FAILURE;
    }

    /* Imprimir el rectángulo */

    return EXIT_SUCCESS;
}

