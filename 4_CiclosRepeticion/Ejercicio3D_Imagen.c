/*
 * Generar los datos de una imagen de un cuadrado en formato ppm
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

int main(void)
{
    int filas, columnas;    // Dimensiones del cuadrado (y de la imagen)
    int rojo, verde, azul;  // Color del cuadrado

    printf("Ingrese las filas y las columnas del rectángulo: ");
    bool valido = (scanf("%d%d", &filas, &columnas) == 2);

    if (!valido)
    {
        puts("Alguno de los números no es válido");
        return EXIT_FAILURE;
    }

    if (filas < 0 || columnas < 0)
    {
        puts("No existen las longitudes menores a 0");
        return EXIT_FAILURE;
    }

    printf("Ahora ingrese la cantidad de rojo verde y azul de la imagen (el máximo es 255 para cada uno)\n");
    valido = (scanf("%d%d%d", &rojo, &verde, &azul) == 3);

    if (!valido)
    {
        puts("Alguno de los números no es válido");
        return EXIT_FAILURE;
    }

    if ((rojo < 0 || rojo > 255) || (verde < 0 || verde > 255) || (azul < 0 || azul > 255))
    {
        puts("Alguno de los colores está fuera de rango");
        return EXIT_FAILURE;
    }

    for (int f = 0; f < filas; f++) {
        for (int c = 0; c < columnas; c++) {

        }
    }

    return EXIT_SUCCESS;
}
