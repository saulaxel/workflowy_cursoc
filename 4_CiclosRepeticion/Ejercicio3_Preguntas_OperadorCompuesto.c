/*
 * Operadores de asignación o compuestos
 */

#include <stdio.h>

int main(void)
{
    int a = 1;
    a += 4; // a = a + 4
    printf("a es: %d\n", a);

    a -= 4; // a = a - 4
    printf("a es: %d\n", a);

    a *= 5; // a = a * 5
    printf("a es: %d\n", a);

    a /= 5;
    printf("a es: %d\n", a);

    a |= 6; // a = a | 6
    printf("a es: %d\n", a);

    a &= 6; // a = a & 6
    printf("a es: %d\n", a);
}
