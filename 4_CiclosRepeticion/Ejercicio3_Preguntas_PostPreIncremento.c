/*
 * Demostrar la diferencia entre preincremento
 * y postincremento
 */

#include <stdio.h>

int main(void)
{
    int var = 5;
    int resultado;

    resultado = var++;

    printf("resultado es: %d\n", resultado--);

    resultado = ++var;

    printf("resultado es: %d\n", resultado);
    printf("--resultado es: %d\n", --resultado);
}
