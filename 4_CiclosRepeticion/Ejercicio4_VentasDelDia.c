/*
 * Programa que sume todos los datos recibidos
 * y muestre el total
 */

#include <stdio.h>
#include <stdbool.h>

int main(void)
{
    float dato, suma;
    bool leido_correctamente;

    leido_correctamente = (scanf("%f", &dato) == 1);
    suma += dato;

    printf("La suma es: %.2f\n", suma); // Se muestra la suma con 2 decimales
}
