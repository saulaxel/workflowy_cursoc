/*
 * Generar tarjetas para anki para
 * memorizar los elementos de la
 * tabla periódica
 */

#include <stdio.h>

int main(void)
{
    int numero;
    char simbolo[3];
    char nombre[64];

    scanf("%d\t%s\t%s", &numero, simbolo, nombre);

    printf("¿Símbolo y Nombre de %d?\t%s - %s\n", numero, simbolo, nombre);
    printf("¿Número y Nombre de %s?\t%d - %s\n", simbolo, numero, nombre);
    printf("¿Número y Símbolo de %s?\t%d - %s\n", nombre, numero, simbolo);
}
