/*
 * A partir de un entero ingresado por el usuario se
 * saca la suma de sus dígitos.
 */

#include <stdio.h>

int main(void)
{
    int entero;

    // Se lee el valor
    printf("Ingrese un entero para sumar sus dígitos: \n");

    while (!scanf("%d", &entero))
    {
        printf("Ingresa un número de verdad >:v\n");

        scanf("%*[^\n]");   // Se eliminan los datos erróneos menos el salto de línea
        scanf("%*c");       // Se elimina el salto de línea
    }

    if (entero < 0)
    {
        entero = -entero; // Si es negativo, lo transformamos a positivo
    }

    // Calcular la suma de sus dígitos
    int suma_digitos = 0;
}
