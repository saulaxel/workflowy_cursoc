/*
 * Generar una imagen PPM de un triángulo
 * rectángulo con la misma altura y ancho.
 */

#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>

#include "funciones_utiles.h"

int main(void)
{
    int alto;

    fprintf(stderr, "Ingresa el alto del triángulo\n");
    validar_condicion(scanf("%d", &alto) && alto > 0);

    printf("P3\n");                 // Formato de la imagen
    printf("%d %d\n", alto, alto);  // Dimensiones de la imagen
    printf("255\n");                // Escala de color al máximo

    printf(BLANCO " ");
    printf(ROJO   " ");
}
