/*
 * Generar una imagen PPM de una línea
 * diagonal a 45% cuyo largo sea especificado por el usuario
 */

#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>

#include "funciones_utiles.h"

int main(void)
{
    int largo;

    fprintf(stderr, "Ingresa el largo de la diagonal\n");
    validar_condicion(scanf("%d", &largo) && largo > 0.0);

    printf("P3\n");                 // Formato de la imagen
    printf("%d %d\n", 200, 200);  // Dimensiones de la imagen
    printf("255\n");                // Escala de color al máximo

    for (; ;)
    {
        for (; ;)
        {
            printf(BLANCO " ");
            printf(ROJO   " ");
        }
    }
}
