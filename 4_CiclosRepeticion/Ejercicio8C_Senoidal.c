/*
 * Generar una imagen PPM del área inferior de una función
 * senoidal
 */

#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>

#include "funciones_utiles.h"

int main(void)
{
    float amplitud, periodo;

    fprintf(stderr, "Ingresa la amplitud de la función senoidal\n");
    validar_condicion(scanf("%f", &amplitud) && amplitud > 0.0);
    fprintf(stderr, "Ingresa el periodo de la senoidal\n");
    validar_condicion(scanf("%f", &periodo) && periodo > 0.0);

    printf("P3\n");                 // Formato de la imagen
    printf("%d %d\n", 200, 200);    // Dimensiones de la imagen
    printf("255\n");                // Escala de color al máximo

    printf(BLANCO " ");
    printf(ROJO   " ");
}

