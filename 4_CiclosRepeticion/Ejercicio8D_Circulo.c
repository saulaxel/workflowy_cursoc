/*
 * Generar una imagen PPM de un círculo cuyo radio sea
 * especificado por el usuario.
 */

#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>

#include "funciones_utiles.h"

int main(void)
{
    int radio;

    fprintf(stderr, "Ingresa el radio del círculo\n");
    validar_condicion(scanf("%d", &radio) && radio > 0);

    printf("P3\n");                             // Formato de la imagen
    printf("%d %d\n", radio * 2, radio * 2);    // Dimensiones de la imagen
    printf("255\n");                            // Escala de color al máximo

    printf(BLANCO " ");
    printf(ROJO   " ");
}
