#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

void limpiar_buffer(void)
{
    scanf("%*[^\n]");
    scanf("%*c");
}

int leer_entero(char mensaje[])
{
    int d;
    bool valido;

    do {
        printf("%s", mensaje);
        valido = scanf("%d", &d) == 1;

        if (!valido)
        {
            limpiar_buffer();
        }
    } while (!valido);

    return d;
}

double leer_flotante(char mensaje[])
{
    double f;
    bool valido;

    do {
        printf("%s", mensaje);
        valido = scanf("%lf", &f) == 1;

        if (!valido)
        {
            limpiar_buffer();
        }
    } while (!valido);

    return f;
}

void validar_condicion(bool condicion)
{
    if (!condicion)
    {
        fprintf(stderr, "Error, saliendo del programa\n");
        exit(EXIT_FAILURE);
    }
}

void validar_con_mensaje(bool condicion, const char mensaje[])
{
    if (!condicion)
    {
        fputs(mensaje, stderr);
        exit(EXIT_FAILURE);
    }
}

void quitar_espacios(char cadena[])
{
    int largo_cad = strlen(cadena);

    for (int i = 0, pos = 0; i < largo_cad; i++)
    {
        if (cadena[i] != ' ' && cadena[i] != '\t')
        {
            cadena[pos++] = cadena[i];
        }
    }
}
