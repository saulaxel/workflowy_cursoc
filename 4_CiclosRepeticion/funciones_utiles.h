#include <stdbool.h>

#define BLANCO "255 255 255"
#define NEGRO  "  0   0   0"

#define ROJO   "255   0   0"
#define VERDE  "  0 255   0"
#define AZUL   "  0   0 255"

void limpiar_buffer(void);
int leer_entero(char mensaje[]);
double leer_flotante(char mensaje[]);
void validar_condicion(bool condicion);
void quitar_espacios(char cadena[]);;
