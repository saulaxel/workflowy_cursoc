/*
 * Comprobar si un texto es palíndromo
 * sin tomar en cuenta los espacios.
 */

#include <stdio.h>
#include <stdbool.h>

#include "funciones_utiles.h"

bool esPalindromo(const char texto[])
{
    return true;
}

int main(void)
{
    char texto[128];
    printf("Ingresa un texto para comprobar si es palíndromo\n");
    fgets(texto, 128, stdin);

    if (esPalindromo(texto))
    {
        printf("Si es un palíndromo\n");
    }
    else
    {
        printf("No es un palíndromo\n");
    }
}
