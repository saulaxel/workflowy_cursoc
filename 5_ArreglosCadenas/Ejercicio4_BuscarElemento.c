/*
 * Llenar un arreglo de 10 elementos de números
 * aleatorios entre 1 y 10 y luego buscar un
 * elemento
 */

#include <stdio.h>
#include <stdlib.h> // para usar srand(), rand()
#include <time.h>   // para usar time()

int main(void)
{
    int arreglo[10];

    srand(time(NULL));

    // Llenar el arreglo
    arreglo[0] = rand() % 10 + 1; // Un aleatorio de 1 y 10

    // Mostrar el arreglo

    printf("El arreglo es: ");
    for (int i = 0; i < 10; i++)
    {
        printf("%d ", arreglo[i]);
    }

    // Buscar un elemento del arreglo
    int buscado;
    int posicion;

    printf("Ingresa un valor para ver si está en el arreglo\n");
    scanf("%d", &buscado);

}
