/*
 * Sacar el promedio de calificación de un grupo y ordenar
 * los alumnos según sus notas.
 */

#include <stdio.h>
#include <stdbool.h>

enum { ALUMNOS_MAXIMOS = 50, MATERIAS = 5 };

#define SEPARADOR "\t"

int main(void)
{
    // Leyendo los datos
    char nombres[ALUMNOS_MAXIMOS][128];
    int calif[ALUMNOS_MAXIMOS][MATERIAS];
    bool datos_validos = true;

    int n = 0;
    do {
        datos_validos = datos_validos && scanf("%127[^" SEPARADOR "]%*c", nombres[n]);

        for (int m = 0; m < MATERIAS; m++)
        {
            datos_validos = datos_validos && scanf("%d", &calif[n][m]);
        }
    } while (datos_validos);

    // Sacando el promedio
    float promedio[n];

    for (int i = 0; i < n; i++)
    {

    }

    // Ordenando los datos

    // Imprimiendo los datos
}

