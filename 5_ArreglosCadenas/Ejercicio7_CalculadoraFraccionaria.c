/*
 * Realizar la suma de dos números fraccionarios
 * y mostrar el resultado como fracción. Se requiere simplificar
 * las fracciones.
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#include "funciones_utiles.h"

#ifdef _WIN32
# define COMANDO_LIMPIAR "cls"
#else
# define COMANDO_LIMPIAR "clear"
#endif

struct racional {
    int numerador;
    int denominador;
};

void suma_fraccion(void);
void resta_fraccion(void);
void multip_fraccion(void);
void divid_fraccion(void);

void leer_fraccion(int n, struct racional * r);

int main(void)
{
    // Leyendo los datos
    puts("\t |      Calculador de fracciones      |\n"
         "\t |                          _______   |\n"
         "\t |       ____   oo    k    |/      |  |\n"
         "\t |      / pi    __ (x)     |      (_) |\n"
         "\t |     / ---  + \\  ---  =  |      \\|/ |\n"
         "\t |   \\/   2     /_  k!     |       |  |\n"
         "\t |              k=1        |      / \\ |\n"
         "\t | Presione enter para continuar ...  |\n"
         "\t <------------------------------------>\n");
    getchar();

    int opcion = -1;

    while (opcion != 5)
    {
        system(COMANDO_LIMPIAR);

        puts(" ¿Qué deseas hacer?\n"
             "\t 1) Sumar fracciones\n"
             "\t 2) Restar fracciones\n"
             "\t 3) Multiplicar fracciones\n"
             "\t 4) Restar fracciones\n"
             "\t 5) Salir del programa\n");

        opcion = leer_entero(1, 5);

        switch (opcion) {
            case 1: suma_fraccion();   break;
            case 2: resta_fraccion();  break;
            case 3: multip_fraccion(); break;
            case 4: divid_fraccion();  break;
            case 5: puts("Vuelva pronto :D"); break;
        }
    }

    struct racional r1; // Para la fracción 1
    struct racional r2; // Para la fracción 2

    leer_fraccion(1, &r1);
    leer_fraccion(2, &r2);

    // Calcular el resultado

    if (r1.denominador == 0 || r2.denominador == 0)
    {
        printf("El resultado no \n");
    }
}

void leer_fraccion(int n, struct racional * r)
{
    bool datos_validos;

    do {
        printf("Ingrese la fracción %d en formato a/b (sin espacios):\n", n);
        datos_validos = (scanf("%d/%d", &((*r).numerador), &((*r).denominador)) == 2);

        if (!datos_validos)
        {
            puts("Error de formato en la fracción");
            scanf("%*[^\n]"); // Se elimina la basura excepto el salto de línea
            scanf("%*c");     // Se elimina el salto de línea
        }
    } while (!datos_validos);
}

void suma_fraccion(void)
{
    // Leer los datos
    struct racional op1, op2;

    leer_fraccion(1, &op1);
    leer_fraccion(2, &op2);

    // Operar y mostrar
    struct racional resultado;
}

void resta_fraccion(void)
{
    // Leer los datos
    struct racional op1, op2;

    leer_fraccion(1, &op1);
    leer_fraccion(2, &op2);

    // Operar y mostrar
    struct racional resultado;
}

void multip_fraccion(void)
{
    // Leer los datos
    struct racional op1, op2;

    leer_fraccion(1, &op1);
    leer_fraccion(2, &op2);

    // Operar y mostrar
    struct racional resultado;
}

void divid_fraccion(void)
{
    // Leer los datos
    struct racional op1, op2;

    leer_fraccion(1, &op1);
    leer_fraccion(2, &op2);

    // Operar y mostrar
    struct racional resultado;
}
