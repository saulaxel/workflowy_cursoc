/*
 * Imprimir las tablas de multiplicar en orden
 * salteado.
 */

#include <stdio.h>

int main(void)
{
    for (int i = 1; i <= 10; i++)
    {
        for (int j = 1; j <= 10; j++)
        {
            printf("N\n");
        }
        putchar('\n');
    }
}
