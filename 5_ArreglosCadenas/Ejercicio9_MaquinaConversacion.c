/*
 * Programa que responda a lo que
 * dice el usuario de al menos 5 formas
 * distintas.
 */

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>  // Funciones para identificar y convertir caracteres
#include <string.h>
#include <stdbool.h>

void convertirMinuscula(char cadena[]);

int main(void)
{
    bool salir;

    printf("Bienvenid@, Humano. ¿Hay algo de lo que quieras hablar?\n");
    do {
        char mensaje[256];

        printf(" > ");
        fgets(mensaje, 256, stdin);
        convertirMinuscula(mensaje);

        if (strcmp(mensaje, "salir") == 0)
        {
            salir = true;
        }
    } while (!salir);
}

void convertirMinuscula(char cadena[])
{
    for (int i = 0; cadena[i]; i++)
    {
        cadena[i] = tolower(cadena[i]); // tolower() convierte una letra a minúscula; Esta función es de "ctype"
    }
}
