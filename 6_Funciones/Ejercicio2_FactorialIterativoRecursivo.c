/*
 * Medir el tiempo que se demora la computadora
 * en calcular los factoriales del 1 al 20 por
 * iteración y por recursividad.
 */

#include <stdio.h>
#include <time.h>

long long factorialIterativo(int n);
long long factorialRecursivo(int n);

int main(void)
{
    clock_t tiempo1, tiempo2;

    puts("Factorial iterativo:");

    tiempo1 = clock(); // Se obtiene el instante de tiempo
    for (int i = 1; i <= 20; i++)
    {
        long long fact = factorialIterativo(i);
        printf("El factorial de %d es: %lld\n", i, fact);
    }
    tiempo2 = clock();

    printf("Tiempo por iteración: %d", (int)(tiempo2 - tiempo1));

    puts("Factorial recursivo: ");

    tiempo1 = clock();
    for (int i = 1; i <= 20; i++)
    {
        long long fact = factorialRecursivo(i);
        printf("El factorial de %d es: %lld\n", i, fact);
    }
    tiempo2 = clock();

    printf("Tiempo por recursión: %d", (int)(tiempo2 - tiempo1));
}

long long factorialIterativo(int n)
{
    return n;
}

long long factorialRecursivo(int n)
{
    return n;
}
