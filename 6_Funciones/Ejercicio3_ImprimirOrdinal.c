/*
 * Imprimir el ordinal a partir de un número
 * entero menor a 1000
 */

#include <stdio.h>
#include <stdlib.h>

void imprimirOrdinal(int n);

int main(void)
{
    int n;
    fprintf(stderr, "Ingrese un número para imprimir su ordinal (1 - 1000): ");

    if (scanf("%d", &n) != 1 || n < 1 || n > 1000)
    {
        fprintf(stderr, "Número incorrecto\n");
        return EXIT_FAILURE;
    }

    imprimirOrdinal(n);
}

void imprimirOrdinal(int n)
{
    static char * separados[] =
    {
        [1] = "primero", "segundo", "tercero", "cuarto",
        "quinto", "sexto", "séptimo", "octavo", "noveno",
        [11] = "undécimo", [12] = "duodécimo"
    };

    static char * esimos[] =
    {
        [1] = "décimo", "vigésimo", "trigésimo", "cuadragésimo",
        "quincuagésimo", "sexagésimo", "septuagésimo", "octogésimo",
        "nonagésimo"
    };

    static char * ntesimos[] =
    {
        [1] = "centésimo", "ducentésimo", "tricentésimo", "cuadrigentésimo",
        "quingentésimo", "exagentésimo", "septingentésimo", "octingentésimo",
        "noningentésimo", "milésimo"
    };

    int unidades, decenas, centenas;
    if (n % 100 == 11 || n % 100 == 12) // Casos especiales
    {
        unidades = n % 100;
        decenas  = 0;

        n /= 100;
    }
    else
    {
        unidades = n % 10;
        n /= 10;
        decenas = n % 10;
        n /= 10;
    }
    centenas = n;

    if (centenas)
    {
        printf("%s ", ntesimos[centenas]);
    }

    if (decenas)
    {
        printf("%s ", esimos[decenas]);
    }

    if (unidades)
    {
        printf("%s", separados[unidades]);
    }

    putchar('\n');  // Un salto de línea
}
