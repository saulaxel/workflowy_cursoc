/*
 * Imprimir la serie de fibonacci hasta un máximo de 50 términos
 * mediante recursión.
 */

#include <stdio.h>
#include <assert.h>

enum { MAXIMO = 50 };

long long fibonacci(int n);

int main(void)
{
    int n;
    printf("¿Cuántos dígitos de fibonacci quieres?\n");
    assert(scanf("%d", &n));

    if (n > MAXIMO || n < 0)
    {
        printf("Cantidad inválida\n");
        return -1;
    }

    for (int i = 1; i < n; i++)
    {
        printf("%lld, ", fibonacci(i));
    }
    printf("%lld ", fibonacci(n));
}

long long fibonacci(int n)
{

}
