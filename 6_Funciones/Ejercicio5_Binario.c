/*
 * Recibir un número en base decimal y pasarlo a
 * binario.
 */

#include <stdio.h>
#include <assert.h>

void imprimirBinario(int n);

int main(void)
{
    int n;
    printf("Ingrese un número: ");
    assert(scanf("%d", &n));

    if (n < 0)
    {
        putchar('-');
        n = -n;
    }

    imprimirBinario(n);
}

void imprimirBinario(int n)
{

}
