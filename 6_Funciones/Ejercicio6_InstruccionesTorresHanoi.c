/*
 * Programa que de instrucciones para resolver
 * las torres de hanoi de altura N.
 */

#include <stdio.h>
#include <assert.h>

void instruccionesTorre(int n);
void auxiliarInstrucciones(int discos, int origen, int aux, int dest);

int main(void)
{
    int n;
    printf("¿Qué altura de torre require? ");
    assert(scanf("%d", &n));

    instruccionesTorre(n);
}

void instruccionesTorre(int n)
{

}

void auxiliarInstrucciones(int discos, int origen, int aux, int dest)
{

}
