/*
 * Dibujar un fractal con forma de árbol usando
 * recursividad.
 */

#include <stdio.h>
#include <math.h>

void dibujarFractal(void);
void dibujarLineaAngulo(int x1, int y1, int largo, double angulo);

int main(void)
{
    dibujarFractal();
}

void dibujarFractal()
{
    dibujarLineaAngulo(300, 10, 200, 45.0);
}

void dibujarLineaAngulo(int x1, int y1, int largo, double angulo)
{
    double angulo_radianes = angulo * M_PI / 180.0;
}
