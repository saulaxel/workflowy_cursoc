/*
 * Demostración de las operaciones que se pueden
 * realizar con apuntadores.
 */

#include <stdio.h>

int main(void)
{
    int entero = 10;
    int *apt_entero = &entero; // El & (ampersand) sirve para obtener
                               // la dirección de una variable y
                               // asignarla a un apuntador

    *apt_entero = 20; // * (asterisco) sirve para acceder al valor
                      // de la variable y (posiblemente) cambiarlo.

    printf("%d\n", entero);

    int arreglo[10] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
    apt_entero = arreglo; // Los arreglos no requieren & para usarse
                          // como apuntador

    for (int i = 0; i < 10; i++) {
        printf("El apuntador contiene: %p\n", apt_entero);
        printf("El valor al que hace referencia es: %d\n", *apt_entero);
        ++apt_entero; // Avanza una posición
    }
}
