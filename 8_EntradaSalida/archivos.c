/*
 * Demostración simple de archivos de entrada y salida
 */

#include <stdio.h>

int main(void)
{
    // Los archivos se abren con fopen()
    FILE *archivo_entrada = fopen("datos.dat", "r");  // r de "read"
    FILE *archivos_salida = fopen("salida.dat", "w"); // w de "write"

    // En lugar de printf() y scanf(), se usarán las funciones
    // fprintf() y fscanf() que tienen un argumento más al inicio.
    // Ese argumento especifica el archivo con el que se opera.

    int a, b, c, d;
    fscanf(archivo_entrada, "%d %d %d %d", &a, &b, &c, &d);

    fprintf(archivos_salida, "%d %d %d %d\n", (a * a), (b * b), (c * c), (d * d));
}
