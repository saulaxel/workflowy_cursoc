/*
 * Definir un tipo de dato struct
 * y declarar variables
 * de la misma en C.
 */

#include <stdio.h>

struct fecha // struct fecha es el nombre del tipo de dato
{
    // dia, mes y anio son los datos miembro
    // cada ves que declaremos una variable "struct fecha" esta contendrá
    // los tres datos.
    int dia;
    int mes;
    int anio;
};

// Una estructura se recibe como cualquier variable
void mostraFecha(struct fecha f)
{
    printf("%02d/%02d/%02d\n", f.dia, f.mes, f.anio);
}

// Un apuntador a estructura tiene un operador extra: -> (flecha)
// Se usa en lugar del punto para acceder a los datos
void escanearFecha(struct fecha *f)
{
    scanf("%d/%d/%d", &f->dia, &f->mes, &f->anio);
}

int main(void)
{
    struct fecha f1 = { 10, 12, 1999 }, f2;

    f2.dia = 5;
    f2.mes = 1;
    f2.anio = 2019;

    mostraFecha(f1);
    mostraFecha(f2);

    struct fecha f3;
    escanearFecha(&f3);
    mostraFecha(f3);
}
