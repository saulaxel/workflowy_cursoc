#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int aleatorio(void)
{
    int map[10] = { 5, 6, 7, 7, 8, 8, 8, 9, 9, 10 };
    int n = rand() % 10;
    return map[n];
}

int main(void)
{
    int cal;
    srand(time(NULL));

    int n;
    scanf("%d", &n);
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < 5; j++) {
            cal = aleatorio();
            printf("%d%s", cal, j != 4 ? "," : "");
        }
        putchar('\n');
    }
}
