
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

float venta_aleatoria(void)
{
    int rango = rand() % 100;

    if (rango < 50)
    {
        return (double)(rand() % (40) + 2) / 2.0;
    }
    else if (rango < 80)
    {
        return (double)(rand() % (60) + 42) / 2.0;
    }
    else if (rango < 95)
    {
        return (double)(rand() % (100) + 102) / 2.0;
    }
    else
    {
        return (double)(rand() % (1800) + 202) / 2.0;
    }
}

int main(void)
{
    srand(time(NULL));

    int cantidad;
    scanf("%d", &cantidad);

    for (int i = 0; i < cantidad; i++)
    {
        float monto_venta = venta_aleatoria();
        printf("%.2f\n", monto_venta);
    }
}
